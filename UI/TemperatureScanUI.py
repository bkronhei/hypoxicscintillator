"""
Instructions on use:
    First, configure the UI to check the desired channels with the desired
    paramters. All paramters betwen lines 96 and 138 can be adjusted. See the
    comments around them for more details. 
    
    To actually execute put this python file in a new directory and navigate
    there in the terminal. Run
        python3 UI.py
    in the directory with the file. This will start the UI and the data
    acquisiton. 
    
    A GUI will pop up when this starts. It will have four graphs which will
    periodically rotate to display all of the measured values. At the bottom
    will be five buttons.
    
    Show Grid:
        This will pop up a grid which will list all the measured paramters. It
        will periodically update
    New Graph:
        This will offer a dropdown of choices for the measurement type. Click 
        on it to select the desired graph. The next choice is the channel. This
        is the 1 indexed value corresponding to the desired graph of the given
        type. Finally, past hours selects how many of the previous hours of data
        should be shown. If nothing is entered here it will plot all of the data.
    Update Bias Voltage:
        This will allow you to enter the Rigol channel to adjust and the range
        range (voltage) it should be set to. Note that this uses an absolute
        numbering system between 1 and 4, so the same Rigol channel will always
        have the same number regardless of what is actually being used.
    Initiate Scan Cycle:
        This allows you to set 12 paramters:
            Cycles: Number of scan cyles to run
            Bias Channel: Which (absolute) bias channel you want to adjust
            LED Channel: Which (absolute) LED channel you want to use
            Temperature Acclimation Time: Time at which the bias LED should sit
                at nominal at the start
            Pedestal Time: Time to measure the pedestal
            LED Warmup Time: Time for the LED to warm up at nominal voltage
            Bias Scan Step: Time for each bias scan step
            Nominal Bias: Nominal value for the bias voltage
            Nominal LED: Nominal value for the LED voltage
            Start Scan Below Nominal: Bias scan starts at Nominal Bias -
                Start Scan Below Nominal
            Run Scan Above Nominal: Bias scan goes up to Nominal Bias +
                Run Scan Above Nominal
            Scan Step Size: Voltage step size of bias scan
    Add note:
        This allows the user to add a note to the log file. Please do not use
        a pipe "|" symbol.
    End scan:
        This removes all tasks from the execution queue
        
        
    To terminate the UI click on the x button at the top right. A pop-up will 
    prompt you to confirm that you really want to exit to prevent an inadvertant
    termination of the UI.
            
    The UI will output save files periodically into a temp folder. Each of
    these files contains only the measurements taken since the last temp file was
    written. They are intended to be used as a backup incase something prevented
    the main save from working. The main saves will come less frequently, but 
    will contain all the data generated so far. All the save files will be in the
    form of a .npy file except for the log files which will be simple text files.
"""

import pyvisa
import matplotlib
import os
import time
import traceback

from collections import deque 

import numpy as np
import tkinter as tk

from tkinter import ttk
from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg, NavigationToolbar2Tk)
from matplotlib.figure import Figure
from matplotlib.backend_bases import key_press_handler

class MainUI(tk.Tk):
    def __init__(self):
        super().__init__()

        
        ### These are the paramters which can be tweaked. They control the
        ### timing of the scans, timing of the saves, and the channels of the scans
        ###
        ###
        ###

        # General parameters
        self.relayDelay = 0.250     # seconds, delay after a relay closes before the measurement
        self.plSync = True          # boolean, whether to sync with the power line
        self.delay = 0              # seconds, minimum time a full scan should take
                                    # only matters if greater than channels*relayDelay

        self.queryTime = 20         # seconds, time interval at which to pull data from the Keithly
                                    # this should be longer thant two full scans  


        self.saveTime = 120         # seconds, time interval at which temp files are written to file        
        self.fullSaveTime = 3600*4  # seconds, interval at which to save full data to file
        
        # Lists of keithly channels organized by type
            
        # Radiation flags, only relavent when at Goddard
        radFlagChannelAssignments = []
        # SiPM output voltage channels, these are the main signals
        sipmVChannelAssignments =  list(range(107, 117))+list(range(127, 132, 1))
        # Channels monitoring the bias voltages supplied to the SiPMs
        biasVoltageChannelAssignments = [134]
        # Channels monitoring the resistance, and therefore temperatures, of the RTDs
        temperatureChannelAssignments = list(range(201, 218, 1))
        # Channels monitoring a current
        currentChannelAssignments = [221]        
               
        # Split the SiPMs into 2 categories, this gives the number in the first
        # This is only used when making the histogram of the different signal voltages
        self.sipmVChannelsCategory1 = len(sipmVChannelAssignments)//2 
         
        
        # Paramters to adjust based on monitor size/resolution
        figsize = (18, 9) # This controls the size of the GUI window. This
                          # may be adjusted depending on the screen size and 
                          # resolution
        self.mainFontSize = 14 # This controls the font size on the main graph.
                               # It may also need adjusted
                               
                               
        self.measurementColumnWidth = 550 # Horizontal length of grid column with the measurement name
        self.valueColumnWidth       = 300 # Horizontal length of grid column with the measurement value
        self.gapColumnWidth         = 150 # Horizontal length of blank grid column
        self.allRowsHeight          = 60  # Vertical height of all grid rows
        
        ###
        ###
        ###
        ### From here on out nothing should be changed without a good reason
        
        
        # system time in seconds of the dmm and computer at start
        self.dmmStartTime = 0
        self.computerStartTime = 0


        # Number of each channel
        self.radFlagChannels = len(radFlagChannelAssignments)
        self.sipmVChannels = len(sipmVChannelAssignments)
        self.biasVoltageChannels = len(biasVoltageChannelAssignments) 
        self.temperatureChannels = len(temperatureChannelAssignments)
        self.currentChannels = len(currentChannelAssignments)

        keithlyVoltageChannels = radFlagChannelAssignments + sipmVChannelAssignments + biasVoltageChannelAssignments
        keithlyResistanceChannels = temperatureChannelAssignments
        keithlyCurrentChannels = currentChannelAssignments
        
        # SCPI setup commands
        self.keithlyAllChannelsString = "(@"
        self.keithlyVoltageString = "(@"
        for voltage in keithlyVoltageChannels:
            self.keithlyVoltageString += str(voltage)+", "
            self.keithlyAllChannelsString += str(voltage)+", "
        self.keithlyVoltageString = self.keithlyVoltageString[:-2]+")"
        
        self.keithlyResistanceString = "(@"
        for temperature in keithlyResistanceChannels:
            self.keithlyResistanceString += str(temperature)+", "
            self.keithlyAllChannelsString += str(temperature)+", "
        self.keithlyResistanceString = self.keithlyResistanceString[:-2]+")"
        
        self.keithlyCurrentString = "(@"
        for current in keithlyCurrentChannels:
            self.keithlyCurrentString += str(current)+", "
            self.keithlyAllChannelsString += str(current)+", "
        self.keithlyCurrentString = self.keithlyCurrentString[:-2]+")"
        self.keithlyAllChannelsString = self.keithlyAllChannelsString[:-2]+")"
        

        # Number of channels of data read from the DMM
        self.channelNumber = self.radFlagChannels + self.sipmVChannels + self.biasVoltageChannels + self.temperatureChannels + self.currentChannels

        # What to do when the window is closed
        self.protocol("WM_DELETE_WINDOW", self.on_closing)

        # Make directory in which to save the data files
        directory = os.getcwd()
        if(not os.path.exists(directory+"/tempFiles")):
            os.makedirs(directory+"/tempFiles")

        # Log File
        self.log_file = open('log'+str(int(time.time()))+'.txt', 'a')

        # UI title
        self.wm_title("Hypoxic Scintilator Test")


        self.data=[]

        self.voltageOutIndices = range(self.radFlagChannels, self.radFlagChannels+self.sipmVChannels)
        self.temperatureIndices = range(self.radFlagChannels+self.sipmVChannels+self.biasVoltageChannels, self.radFlagChannels+self.sipmVChannels+self.biasVoltageChannels+self.temperatureChannels)
        self.currentOutIndices = range(max(self.temperatureIndices)+1,max(self.temperatureIndices)+1+self.currentChannels)
        self.cableResistances = [4.5]*self.temperatureChannels


        font = {'size': self.mainFontSize}
        matplotlib.rc('font', **font)
        matplotlib.rc('xtick', labelsize=self.mainFontSize)
        matplotlib.rc('ytick', labelsize=self.mainFontSize)

        # Number of times data has been taken from the DMM
        self.counter = 0

        # First measurement to save next time data is written to file
        self.saveIndex = 0
        self.rigolSaveIndex = 0
        self.rigolData = []

        # Create the area in which the four rotating graphs are displayed
        self.fig = matplotlib.figure.Figure(figsize=figsize)
        
        
        self.ax = [self.fig.add_subplot(221), self.fig.add_subplot(222),
                   self.fig.add_subplot(223), self.fig.add_subplot(224)]
        self.canvas = FigureCanvasTkAgg(self.fig, master=self)
        self.canvas.draw()
        self.canvas.get_tk_widget().grid(row=0, column=0, columnspan=6)

        # Create buttons on the GUI
        gridButton = tk.Button(master=self, text="Show Grid ",
                               command=self.gridFunction, font=(None, self.mainFontSize))
        graphButton = tk.Button(master=self, text="New Graph ",
                                command=self.graphFunction, font=(None, self.mainFontSize))
        updateOffsetButton = tk.Button(master=self,
                                       text="Update Bias Voltage ",
                                       command=self.biasFunction,
                                       font=(None, self.mainFontSize))
        voltageScanButton = tk.Button(master=self,
                                      text="Initiate Scan Cycle",
                                      command=self.biasScanFunction,
                                      font=(None, self.mainFontSize))
        noteButton = tk.Button(master=self,
                               text="Add note ",
                               command=self.noteFunction,
                               font=(None, self.mainFontSize))
        
        endScanButton = tk.Button(master=self,
                                  text="End Scan ",
                                  command=self.endScan,
                                  font=(None, self.mainFontSize))

        # Assign locations to the buttons
        gridButton.grid(row=2, column=0, columnspan=1, sticky="NESW")
        graphButton.grid(row=2, column=1, columnspan=1, sticky="NESW")
        updateOffsetButton.grid(row=2, column=2, columnspan=1, sticky="NESW")
        voltageScanButton.grid(row=2, column=3, columnspan=1, sticky="NESW")
        noteButton.grid(row=2, column=4, columnspan=1, sticky="NESW")
        endScanButton.grid(row=2, column=5, columnspan=1, sticky="NESW")

        # Store the DMM ranges and power supply voltages in case of loss
        # of connection
        self.ranges = {}
        self.psValues = {"PS1C1": 0.0, "PS1C2": 0.0}



        graphs = []
        for x in range(self.radFlagChannels):
            graphs.append("R"+str(x+1))
        for x in range(self.sipmVChannels):
            graphs.append("V"+str(x+1))
        for x in range(self.biasVoltageChannels):
            graphs.append("P"+str(x+1))
        for x in range(self.temperatureChannels):
            graphs.append("T"+str(x+1))
        for x in range(self.currentChannels):
            graphs.append("C"+str(x+1))
        
        while(len(graphs)%4!=0):
            graphs.append("H1")
        self.graphCycleLength = len(graphs)//4

        # Contains the order for the graphs to appear
        self.graphCycle = []
        self.graphCycle.append(graphs[0*self.graphCycleLength:1*self.graphCycleLength])
        self.graphCycle.append(graphs[1*self.graphCycleLength:2*self.graphCycleLength])
        self.graphCycle.append(graphs[2*self.graphCycleLength:3*self.graphCycleLength])
        self.graphCycle.append(graphs[3*self.graphCycleLength:4*self.graphCycleLength])
        self.graphCycleLocation = 0

        # Setup and start running the scan
        self.initializeScan()

        # Create the graphs after 10 seconds to allow data to accumulate
        self.after(10000, self.rotateGraphs)
        
        # Create queue holding taskss
        self.taskQueue = deque()
        self.queueRunning = False
        
    def initializeScan(self):
        """
        Creates a connection with the DMM and power supplies, sets the
        measurement ranges to their proper values, creates a scan, and then
        runs the scan. This is used at initialization of the object and any
        time a connection is dropped.

        Returns
        -------
        None.
        for x in range(4):

        """

        # Determine the buffer size
        scansPerBuffer = 10000  # This value should maybe be tweeked
        self.bufferSize = scansPerBuffer * self.channelNumber
        self.previousLocation = 1  # Starting point to take new data
        self.rm = pyvisa.ResourceManager()

        # Verify that theer are three visa connections. If not, launch recovery
        # until there are three.
        allAddresses = self.rm.list_resources()
        print(allAddresses)
        if(len(allAddresses) != 3):
            timeVal = str(int(time.time()))
            self.log_file.write('MissingVISAAdress|0|FAIL|' + timeVal + "\n")
            self.recovery()
            # Recovery calls this function again, so no further action is
            # needed here if recovery is launched
        else:
            # Determine VISA addresses for all three devices
            for val in allAddresses:
                if("DP8G231800056" in val):
                    self.visaAddressPS1 = val
                else:
                    self.visaAddressDMM = val

            # Open up all three VISA connections and reset the devices
            self.dmm = self.rm.open_resource(self.visaAddressDMM)
            self.dmm.write("*RST")
            self.ps1 = self.rm.open_resource(self.visaAddressPS1)
            self.ps1.write("*RST")

            # Set the power supply units to their proper voltages
            self.setPS(1, 1, self.psValues["PS1C1"])
            self.setPS(1, 2, self.psValues["PS1C2"])

            # Turn on the outputs of the power supply units
            self.ps1.write("OUTP CH1, ON")
            self.ps1.write("OUTP CH2, ON")

            # Configure the channels of the DMM
            self.dmm.write(':TRAC:POIN '+str(self.bufferSize)+',"defbuffer1"')
            self.dmm.write('TRACe:FILL:MODE CONT, "defbuffer1"')
            self.dmm.write(':ROUT:SCAN:BUFF "defbuffer1"')
            if(len(self.voltageOutIndices)>0):
                self.dmm.write("FUNC 'VOLT:DC', " + self.keithlyVoltageString)
                self.dmm.write("VOLT:DC:RANG:AUTO ON, " + self.keithlyVoltageString)
                if(self.plSync):
                    self.dmm.write("VOLT:DC:LINE:SYNC ON, " + self.keithlyVoltageString)
                else:
                    self.dmm.write("VOLT:DC:LINE:SYNC OFF, " + self.keithlyVoltageString)
                
                #self.dmm.write("VOLT:DC:RANG:AUTO ON, (@106:109)")
                #self.dmm.write("VOLT:DC:RANG 1, (@103:105)")
                self.dmm.write("VOLT:DC:NPLC 1, " + self.keithlyVoltageString)
            
            if(len(self.temperatureIndices)>0):
                self.dmm.write("FUNC 'RES', " + self.keithlyResistanceString)
                
                if(self.plSync):
                    self.dmm.write("RES:LINE:SYNC ON, " + self.keithlyResistanceString)
                else:
                    self.dmm.write("RES:LINE:SYNC OFF, " + self.keithlyResistanceString)
                
                self.dmm.write("RES:RANG:AUTO ON, " + self.keithlyResistanceString)
                
            if(len(self.currentOutIndices)>0):
                
                self.dmm.write("FUNC 'CURR:DC', " + self.keithlyCurrentString)
                self.dmm.write("CURR:DC:RANG:AUTO ON, " + self.keithlyCurrentString)
                self.dmm.write("CURR:DC:NPLC 1, " + self.keithlyCurrentString)
                if(self.plSync):
                    self.dmm.write("CURR:DC:LINE:SYNC ON, " + self.keithlyCurrentString)
                else:
                    self.dmm.write("CURR:DC:LINE:SYNC OFF, " + self.keithlyCurrentString)
            # Set the ranges for the DMM measurements
            for key in self.ranges.keys():
                self.setRange(int(key), self.ranges[key])

            # Get the dmm system time
            dmmTime = self.dmm.query("SYST:TIME?")
            if(len(dmmTime) > 0):
                dmmTime = dmmTime[:-1]
            # Get the computer system time
            timeVal = str(int(time.time()))
            # Log the time synch information
            if(len(dmmTime) > 0):
                message = "TimeSynch|" + dmmTime + "|SUCCESS|" + timeVal+"\n"
                self.log_file.write(message)
            else:
                message = "TimeSynch|" + dmmTime + "|FAIL|" + timeVal+"\n"
                self.log_file.write(message)

            # Determine the offset between the DMM system time and experiment
            # start time
            dmmTime = float(dmmTime)
            timeVal = float(timeVal)
            if(self.dmmStartTime == 0):
                self.computerStartTime = timeVal
                self.dmmStartTime = dmmTime
                self.startTimeOffset = timeVal - dmmTime
            currentTimeOffset = timeVal - dmmTime
            self.timeShift = currentTimeOffset - self.dmmStartTime
            self.timeShift = self.timeShift - self.startTimeOffset

            # Create and run a scan
            self.dmm.write("ROUT:SCAN " + self.keithlyAllChannelsString)
            self.dmm.write("ROUT:SCAN:COUN:SCAN 0")
            self.dmm.write("ROUT:SCAN:INT "+ str(self.delay))
            self.dmm.write(":ROUTe:DELay "+str(self.relayDelay)+", (@slot1)")
            self.dmm.write(":ROUTe:DELay "+str(self.relayDelay)+", (@slot2)")
            self.dmm.write("TRACe:CLEar")
            self.dmm.write("INIT")

            # Get data from the DMM, this will be called every 2 seconds
            self.after(1000*self.queryTime, self.updateData)
            

    def updateData(self):
        """
        Loads data from the dmm and uses it to update the data stored in RAM.
        Saves a portion of data to the drive every n queries where
        n = saveTime//queryTime.
        Saves all the data to drive every n queries where
        n = saveTime//fullSaveTime
        If the DMM cannot be read, launches recovery

        Returns
        -------
        None.

        """

        # Query the DMM
        tmpBuff, success = self.readDMM()
        # Assuming the DMM waas sucessfully read,
        if(success):
            # Update the data in RAM
            if(len(self.data) == 0):
                self.data = tmpBuff
            else:
                self.data = np.concatenate([self.data, tmpBuff], axis=1)

            # Determine whether the data should be written to disk
            if((self.counter+1) % (self.saveTime//self.queryTime) == 0):
                np.save("tempFiles/temp_"+str(int(time.time()))+".npy",
                        self.data[:, self.saveIndex:])
                self.saveIndex = self.data.shape[1]
            if((self.counter+1) % (self.fullSaveTime//self.queryTime) == 0):
                np.save("full_"+str(int(time.time()))+".npy", self.data)

            # Update counter of DMM queries
            self.counter += 1

            # Schedule the next DMM query
            self.after(1000*self.queryTime, self.updateData)
        else:  # If it wasn't, launch into recovery mode and log it
            message = 'DMMReadFail|0|FAIL|' + str(int(time.time())) + "\n"
            self.log_file.write(message)
            self.recovery()

        # Query the Rigol
        tmpBuff, success = self.readRigol()

        # Assuming the DMM waas sucessfully read,
        if(success):
            tmpBuff = np.reshape(tmpBuff, (7, 1))
            tmpBuff = np.float32(tmpBuff)
            # Update the data in RAM
            if(len(self.rigolData) == 0):
                self.rigolData = tmpBuff
            else:
                self.rigolData = np.concatenate([self.rigolData, tmpBuff],
                                                axis=1)
            # Determine whether the data should be written to disk
            if((self.counter+1) % (self.saveTime//self.queryTime) == 0):
                np.save("tempFiles/tempRigol_"+str(int(time.time()))+".npy",
                        self.rigolData[:, self.rigolSaveIndex:])
                self.rigolSaveIndex = self.rigolData.shape[1]
            if((self.counter+1) % (self.fullSaveTime//self.queryTime) == 0):
                np.save("fullRigol_"+str(int(time.time()))+".npy",
                        self.rigolData)

        else:  # If it wasn't, launch into recovery mode and log it
            message = 'RigolReadFail|0|FAIL|' + str(int(time.time())) + "\n"
            self.log_file.write(message)
            self.recovery()

    def readDMM(self):
        """
        Querries the DMM to get the most recent data measured.

        Returns
        -------
        data: Float32 Array
            An array of double values containg the most recent data extracted
            from the DMM
        sucees: Boolean
            A single Boolean denoting whether data was sucessfully extracted
            from the DMM

        """

        # Default is failure to read DMM
        success = False

        # Catch errors when communicating with the DMM
        try:
            # Get the number of completed scans
            lastIndex = self.dmm.query("ROUTe:SCAN:STATe?")
            lastIndex = lastIndex.split(";")[1]

            # Convert to number of measurements
            lastIndex = int(lastIndex)*self.channelNumber

            # Determine index within the buffer
            lastIndex = lastIndex % self.bufferSize
            if(lastIndex == 0):
                lastIndex = self.bufferSize
            if(lastIndex < self.previousLocation):
                lastIndex = self.bufferSize
            # SCPI command to get the data from the DMM
            command = 'TRACe:DATA? ' + str(self.previousLocation) + ', '
            command += str(lastIndex) + ', "defbuffer1", READ, CHAN, SEC'

            # Convert the data taken from the DMM to a usable shape
            tmpBuff = self.dmm.query(command)
            tmpBuff = tmpBuff[:-1]
            tmpBuff = np.array(tmpBuff.split(","))
            tmpBuff = np.reshape(tmpBuff, (len(tmpBuff)//3, 3))
            tmpBuff = np.float64(tmpBuff)
            # Apply the time shift
            tmpBuff[:, 2] = tmpBuff[:, 2] + self.timeShift
            # Note location of last data extraction
            self.previousLocation = lastIndex+1
            self.previousLocation = self.previousLocation % self.bufferSize
            # Remove unncesary information
            tmpBuff = np.concatenate([tmpBuff[:, 0:1], tmpBuff[:, 2:3]],
                                     axis=1)

            # Get the data into the proper shape for the rest of the program
            data = []
            dataChunkSize = tmpBuff.shape[0]//(self.channelNumber)
            for x in range(dataChunkSize):
                index1 = x*self.channelNumber
                index2 = (x+1)*self.channelNumber
                dataSize = self.channelNumber*2
                dataChunk = tmpBuff[index1:index2, :].reshape(dataSize, 1)
                data.append(dataChunk)
            data = np.concatenate(data, axis=1)
            for (index, res) in zip(self.temperatureIndices, self.cableResistances):
                data[2*index,:] = self.calculateTemperature(data[2*index,:]-res)
            
            for index in self.voltageOutIndices:
                data[2*index,:] = data[2*index,:]*1000
                
            for index in self.currentOutIndices:
                data[2*index,:] = data[2*index,:]*1000000
            
            success = True
        except:  # Something went wrong
            traceback.print_exc()
            data = np.float32(np.array([1]))
        return(data, success)

    def calculateTemperature(self, resistance):
        """
        Converts a resistance to a temperature.

        Parameters
        ----------
        resistance : Float32
            The resistance to convert

        Returns
        -------
        temeperature : Float32
            The temperature corresponding to the given resistance

        """

        a = -246.2150271682803
        b = 0.023654686381224133
        c = 9.668176211494519e-08

        resistance = resistance*10
        temperature = a + b*resistance + c*(resistance**2)
        return(temperature)

    def readRigol(self):
        """
        Querries the Rigol to get the output voltages, current, and power.

        Returns
        -------
        data: Float Array
            An array of values containg the most recent data extracted
            from the Rigols
        sucees: Boolean
            A single Boolean denoting whether data was sucessfully extracted
            from the Rigols
        """
        success = False
        # Get the time
        data = [time.time()-self.computerStartTime]
        # Measure the 2 channels
        reading = self.ps1.query(":MEAS:ALL? CH1")
        data += reading.split(",")
        reading = self.ps1.query(":MEAS:ALL? CH2")
        data += reading.split(",")
        # Check if the data is the correct shape
        if(len(data) == 7):
            success = True
        return(data, success)

    def recovery(self):
        """
        Recovery code to be run in the event that a connection to a power
        supply unit or the DMM is lost. This code checks for VISA connections
        every 2 connections until if finds 3. After this, it calls the
        initializeScan function and allows data taking to resuem.

        Returns
        -------
        None.

        """
        #return()
        recovering = True
        if(self.saveIndex!=self.data.shape[1]):
            np.save("tempFiles/temp_"+str(int(time.time()))+".npy",
                    self.data[:, self.saveIndex:])
            self.saveIndex = self.data.shape[1]
        
        
        # Run a while loop until connections have recovered
        while(recovering):
            print("In Recovery Loop")
            # Check for number of VISA connections
            allAddresses = self.rm.list_resources()
            timeVal = str(int(time.time()))
            if(len(allAddresses) != 2):
                # If there aren't three connections, log it, then sleep
                # for two seconds
                self.log_file.write('Recovery|0|FAIL|' + timeVal+"\n")
                time.sleep(2)
            else:
                # If there are three connections, log it, then close the old
                # DMM connections and try again
                recovering = False
                self.log_file.write('Recovery|0|SUCCESS|' + timeVal+"\n")

                # If there are issues with some of the connections they
                # may already be closed which could throw an error.
                # This handles those possible errors.
                for connection in [self.dmm, self.ps1]:
                    try:
                        connection.close()
                    except:
                        pass
        
        # Having recovered the connection run initializeSan again
        self.initializeScan()

    def setPS(self, unit, channel, value):
        """
        Sets a power supply channel to the requested value if it is allowed,
        does nothing otherwise.

        If the values are allowed but the power supply fails to respond then
        recovery is launched and the original function call is called up again

        Parameters
        ----------
        unit : Int
            The power supply unit to adjust
        channel : Int
            The power supply channel to adjust
        value : Double
            The new max voltage to set for the channel

        Returns
        -------
        None.

        """

        # Verify that the combination of units, channels, and value is allowed
        if(unit == 1):
            if(channel == 1):
                if(value > 60 or value < 0):
                    return()
            if(channel == 2):
                if(value > 8 or value < 0):
                    return()
            else:
            	return()
        else:
        	return()
        
        # Blocks to handle errors
        try:
            # Set the new voltage and check that it is the same
            ps = [self.ps1]
            message = ":APPL CH" + str(channel) + "," + str(value) + ",1"
            ps[unit-1].write(message)
            voltage = ps[unit-1].query(":APPL? CH"+str(channel))
            voltage = voltage.split(",")
            if(len(voltage) == 3):  # Verify the form of the PS output
                voltage = voltage[1]
                if(float(voltage) == value):  # Voltage set properly
                    message = 'PS' + str(unit) + 'C' + str(channel) + '|'
                    message += str(value) + '|SUCCESS|'
                    message += str(int(time.time())) + "\n"
                    self.log_file.write(message)
                    key = 'PS' + str(unit) + 'C' + str(channel)
                    self.psValues[key] = value
                else:  # Voltage set incorrectly
                    message = 'PS' + str(unit) + 'C' + str(channel) + '|'
                    message += voltage[1] + '|PARTIALFAIL|'
                    message += str(int(time.time())) + "\n"
                    self.log_file.write(message)
                    key = 'PS' + str(unit) + 'C' + str(channel)
                    self.psValues[key] = float(voltage)
            else:  # Output of PS not the correct form
                message = 'PS' + str(unit) + 'C' + str(channel) + '|'
                message += str(value) + '|FAIL|' + str(int(time.time()))
                message += "\n"
                self.log_file.write(message)
        except:  # PS fails to respond
            message = 'PS' + str(unit) + 'C' + str(channel) + '|' + str(value)
            message += '|FAIL|' + str(int(time.time()))+"\n"
            self.log_file.write(message)
            self.recovery()
            self.updateData(unit, channel, value)
        

    def gridFunction(self):
        """Instantiate the Grid object to display recent data."""
        GridData(self, self.radFlagChannels, self.sipmVChannels, self.biasVoltageChannels, self.temperatureChannels, self.currentChannels)

    def biasFunction(self):
        """Creates a Form object to adjust bias voltage."""
        Form(self, ["Voltage"], ["Channel", "Range"], "bias")

    def biasScanFunction(self):
        """Creates a Form object to adjust bias voltage."""
        Form(self, ["Scan"], ["Cycles", "Bias Channel", "LED channel", "Temperature Acclimation Time (minutes)",
                              "Pedestal Time (minutes)", "LED Warmup Time (minutes)", "Bias Scan step (minutes)",
                              "Nominal Bias (V)", "Nominal LED (V)", "Start Scan Below Nominal (V)",
                              "Run Scan Above Nominal (V)", "Scan Step Size (V)"], "biasScan")

    def graphFunction(self):
        """Creates a Form object to create a custom graph."""
        Form(self, ["Current", "SIPM Output Voltage", "Temperature",
                    "Radiation", "Power Supply Voltage", "Voltage Histogram"],
             ["Channel", "Past Hours"], "graph")

    def noteFunction(self):
        """Create a Note object to allow the user to create a note."""
        Note(self)

    def rotateGraphs(self):
        """
        Code which updates the four graphs displayed in the main UI. This code
        is called again five seconds later

        Returns
        -------
        None.

        """
        for x in range(4):
            # Check the four graph codes at the current location in the graph
            # cycle and plot the appropriate graphs
            graphCode = self.graphCycle[x][self.graphCycleLocation]
            if(graphCode[0] == "V"):
                self.voltageGraph(int(graphCode[1]), -1, x)
            elif(graphCode[0] == "T"):
                self.temperatureGraph(int(graphCode[1]), -1, x)
            elif(graphCode[0] == "C"):
                self.currentGraph(int(graphCode[1]), -1, x)
            elif(graphCode[0] == "P"):
                self.powerSupplyGraph(int(graphCode[1]), -1, x)
            elif(graphCode[0] == "R"):
                self.radiationFlagGraph(int(graphCode[1]), -1, x)
            elif(graphCode[0] == "H"):
                self.voltageHist(x)

        # Update the location in the graph cycle for next function call
        self.graphCycleLocation += 1
        self.graphCycleLocation = self.graphCycleLocation % self.graphCycleLength
        self.after(10000, self.rotateGraphs)
                
    def executeTask(self):
        if(len(self.taskQueue)>0):
            task = self.taskQueue.popleft()
            task.execute()
            print(task.getDescription())
            self.after(task.getDuration(), self.executeTask)
            self.queueRunning = True
        self.queueRunning = False

    def endScan(self):
        print("Clearing Task Queue")
        self.taskQueue.clear()

    def voltageGraph(self, channelNumber, offset, graphNumber):
        """
        Creates a graph of a SIPM output voltage channel.

        Parameters
        ----------
        channelNumber : int
            The channel of the SIPM output voltage to display
        offset : int
            The past number of hours to show, defaults to all data if 0.
        graphNumber :  int
            The location to graph this. 0-3 correspond to the main GUI, 4
            corresponds to a popout window.

        Returns
        -------
        None

        """
        if(channelNumber not in range(1, 1+self.sipmVChannels)):  # Verify we have a valid channel
            return()
        if(len(self.data) > 0):  # Verify we have data
            # Get the data and the graph ranges
            voltageVals = self.data[2*(self.radFlagChannels+channelNumber-1), :]
            timeVals = self.data[2*(self.radFlagChannels+channelNumber-1)+1, :]
            maxTime = max(timeVals)*1.001
            if(offset < 0):
                minTime = 0
            else:
                minTime = max([maxTime-3600*offset, min(timeVals)])
            minY = min(voltageVals)*0.9
            maxY = max(voltageVals)*1.1
            if(graphNumber == 4):  # Make a popout graph
                title = "SIPM Output Voltage vs. Time for Channel "
                title += str(channelNumber)
                xlabel, ylabel = "Time (s)", "Voltage (V)"
                channel = 2*(self.radFlagChannels+channelNumber-1)
                data = [timeVals, voltageVals]
                xlim, ylim = [minTime, maxTime], [minY, maxY]
                graphType, source = "plot", self
                Graph(title, xlabel, ylabel, channel, data, xlim, ylim,
                      graphType, source)
            else:  # Make the graph on the main GUI
                self.ax[graphNumber].clear()
                self.ax[graphNumber].scatter(timeVals, voltageVals)
                self.ax[graphNumber].set_xlim(minTime, maxTime)
                self.ax[graphNumber].set_ylim(minY, maxY)
                self.ax[graphNumber].set_xlabel("Time (s)")
                self.ax[graphNumber].set_ylabel("Voltage (V)")
                title = "SIPM Output Voltage vs. Time for Channel "
                title += str(channelNumber)
                self.ax[graphNumber].set_title(title)
                self.ax[graphNumber]
                self.fig.tight_layout()
                self.canvas.draw()

    def powerSupplyGraph(self, channelNumber, offset, graphNumber):
        """
        Creates a graph of a power supply output voltage channel.

        Parameters
        ----------
        channelNumber : int
            The channel of the SIPM output voltage to display
        offset : int
            The past number of hours to show, defaults to all data if 0.
        graphNumber :  int
            The location to graph this. 0-3 correspond to the main GUI, 4
            corresponds to a popout window.

        Returns
        -------
        None

        """
        if(channelNumber not in range(1,1+self.biasVoltageChannels)):  # Verify we have a valid channel
            return()
        if(len(self.data) > 0):  # Verify we have data
            # Get the data and the graph ranges
            voltageVals = self.data[2*(self.radFlagChannels+self.sipmVChannels+channelNumber-1), :]
            timeVals = self.data[2*(self.radFlagChannels+self.sipmVChannels+channelNumber-1)+1, :]
            maxTime = max(timeVals)*1.001
            if(offset < 0):
                minTime = 0
            else:
                minTime = max([maxTime-3600*offset, min(timeVals)])
            minY = min(voltageVals)*0.99
            maxY = max(voltageVals)*1.01
            if(graphNumber == 4):  # Make a popout graph
                title = "Power Supply Input Voltage vs. Time for Channel "
                title = title + str(channelNumber)
                xlabel, ylabel = "Time (s)", "Voltage (V)"
                channel = 2*(self.radFlagChannels+self.sipmVChannels+channelNumber-1)
                data = [timeVals, voltageVals]
                xlim, ylim = [minTime, maxTime], [minY, maxY]
                graphType, source = "powerSupply", self
                Graph(title, xlabel, ylabel, channel, data, xlim, ylim,
                      graphType, source)
            else:  # Make the graph on the main GUI
                self.ax[graphNumber].clear()
                self.ax[graphNumber].scatter(timeVals, voltageVals)
                self.ax[graphNumber].set_xlim(minTime, maxTime)
                self.ax[graphNumber].set_ylim(minY, maxY)
                self.ax[graphNumber].set_xlabel("Time (s)")
                self.ax[graphNumber].set_ylabel("Voltage (V)")
                title = "Power Supply Input Voltage vs. Time for Channel "
                title = title + str(channelNumber)
                self.ax[graphNumber].set_title(title)
                self.canvas.draw()

    def temperatureGraph(self, channelNumber, offset, graphNumber):
        """
        Creates a graph of a temperature channel.

        Parameters
        ----------
        channelNumber : int
            The channel of the SIPM output voltage to display
        offset : int
            The past number of hours to show, defaults to all data if 0.
        graphNumber :  int
            The location to graph this. 0-3 correspond to the main GUI, 4
            corresponds to a popout window.

        Returns
        -------
        None

        """
        if(channelNumber not in range(1,1+self.temperatureChannels)):  # Verify we have a valid channel
            return()
        if(len(self.data) > 0):  # Verify we have data
            # Get the data and the graph ranges
            temperatureVals = self.data[2*(self.radFlagChannels+self.sipmVChannels+self.biasVoltageChannels+channelNumber-1), :]
            timeVals = self.data[2*(self.radFlagChannels+self.sipmVChannels+self.biasVoltageChannels+channelNumber-1)+1, :]
            maxTime = max(timeVals)*1.001
            if(offset < 0):
                minTime = 0
            else:
                minTime = max([maxTime-3600*offset, min(timeVals)])
            minY = min(temperatureVals)*0.99
            maxY = max(temperatureVals)*1.01
            if(graphNumber == 4):  # Make a popout graph
                title = "Temperature vs. Time for Channel " + str(channelNumber)
                xlabel, ylabel = "Time (s)", "Temperature (C)"
                channel = 2*(self.radFlagChannels+self.sipmVChannels+self.biasVoltageChannels+channelNumber-1)
                data, xlim = [timeVals, temperatureVals], [minTime, maxTime]
                ylim, graphType, source = [minY, maxY], "plot", self
                Graph(title, xlabel, ylabel, channel, data, xlim, ylim,
                      graphType, source)
            else:  # Make the graph on the main GUI
                self.ax[graphNumber].clear()
                self.ax[graphNumber].scatter(timeVals, temperatureVals)
                self.ax[graphNumber].set_xlim(minTime, maxTime)
                self.ax[graphNumber].set_ylim(minY, maxY)
                self.ax[graphNumber].set_xlabel("Time (s)")
                self.ax[graphNumber].set_ylabel("Temperature (C)")
                title = "Temperature vs. Time for Channel " + str(channelNumber)
                self.ax[graphNumber].set_title(title)
                self.canvas.draw()

    def currentGraph(self, channelNumber, offset, graphNumber):
        """
        Creates a graph of a current channel.

        Parameters
        ----------
        channelNumber : int
            The channel of the SIPM output voltage to display
        offset : int
            The past number of hours to show, defaults to all data if 0.
        graphNumber :  int
            The location to graph this. 0-3 correspond to the main GUI, 4
            corresponds to a popout window.

        Returns
        -------
        None

        """
        if(channelNumber not in range(1,1+self.currentChannels)):  # Verify we have a valid channel
            return()
        if(len(self.data) > 0):  # Verify we have data
            # Get the data and the graph ranges
            currentVals = self.data[2*(self.radFlagChannels+self.sipmVChannels+self.biasVoltageChannels+self.temperatureChannels+channelNumber-1), :]
            timeVals = self.data[2*(self.radFlagChannels+self.sipmVChannels+self.biasVoltageChannels+self.temperatureChannels+channelNumber-1)+1, :]
            maxTime = max(timeVals)*1.001
            if(offset < 0):
                minTime = 0
            else:
                minTime = max([maxTime-3600*offset, min(timeVals)])
            minY = min(currentVals)*0.99
            maxY = max(currentVals)*1.01
            if(graphNumber == 4):  # Make a popout graph
                title = "Current vs. Time for Channel " + str(channelNumber)
                xlabel, ylabel = "Time (s)", "Current (A)"
                channel = 2*(self.radFlagChannels+self.sipmVChannels+self.biasVoltageChannels+self.temperatureChannels+channelNumber-1)
                data, xlim = [timeVals, currentVals], [minTime, maxTime]
                ylim, graphType, source = [minY, maxY], "plot", self
                Graph(title, xlabel, ylabel, channel, data, xlim, ylim,
                      graphType, source)
            else:  # Make the graph on the main GUI
                self.ax[graphNumber].clear()
                self.ax[graphNumber].scatter(timeVals, currentVals)
                self.ax[graphNumber].set_xlim(minTime, maxTime)
                self.ax[graphNumber].set_ylim(minY, maxY)
                self.ax[graphNumber].set_xlabel("Time (s)")
                self.ax[graphNumber].set_ylabel("Current (micro A)")
                title = "Current vs. Time for Channel " + str(channelNumber)
                self.ax[graphNumber].set_title(title)
                self.canvas.draw()

    def radiationFlagGraph(self, channelNumber, offset, graphNumber):
        """
        Creates a graph of a radiation flag channel.

        Parameters
        ----------
        channelNumber : int
            The channel of the SIPM output voltage to display
        offset : int
            The past number of hours to show, defaults to all data if 0.
        graphNumber :  int
            The location to graph this. 0-3 correspond to the main GUI, 4
            corresponds to a popout window.

        Returns
        -------
        None

        """
        if(channelNumber not in range(1,1+self.radFlagChannels)):  # Verify we have a valid channel
            return()

        if(len(self.data) > 0):  # Verify we have data
            # Get the data and the graph ranges
            voltageVals = self.data[2*(channelNumber-1), :]
            timeVals = self.data[2*(channelNumber-1)+1, :]
            maxTime = max(timeVals)*1.001
            if(offset < 0):
                minTime = 0
            else:
                minTime = max([maxTime-3600*offset, min(timeVals)])
            minY = min(voltageVals)*0.99
            maxY = max(voltageVals)*1.01
            if(graphNumber == 4):  # Make a popout graph
                title = "Radiation Flag " + str(channelNumber)
                title += " Voltage vs. Time"
                xlabel, ylabel = "Time (s)", "Voltage (V)"
                channel, data = 2*(channelNumber-1), [timeVals, voltageVals]
                xlim, ylim = [minTime, maxTime], [minY, maxY]
                graphType, source = "plot", self
                Graph(title, xlabel, ylabel, channel, data, xlim, ylim,
                      graphType, source)
            else:  # Make the graph on the main GUI
                self.ax[graphNumber].clear()
                self.ax[graphNumber].scatter(timeVals, voltageVals)
                self.ax[graphNumber].set_xlim(minTime, maxTime)
                self.ax[graphNumber].set_ylim(minY, maxY)
                self.ax[graphNumber].set_xlabel("Time (s)")
                self.ax[graphNumber].set_ylabel("Voltage (V)")
                title = "Radiation Flag " + str(channelNumber)
                title += " Voltage vs. Time"
                self.ax[graphNumber].set_title(title)
                self.canvas.draw()

    def voltageHist(self, graphNumber):
        """
        Creates a histogram of the most recent SIPM output voltages

        Parameters
        ----------
        channelNumber : int
            The channel of the SIPM output voltage to display
        offset : int
            The past number of hours to show, defaults to all data if 0.
        graphNumber :  int
            The location to graph this. 0-3 correspond to the main GUI, 4
            corresponds to a popout window.

        Returns
        -------
        None

        """
        if(len(self.data) > 0):  # Verify we have data
            # Get the data and the graph ranges
            voltages1 = []
            voltages2 = []
            for x in range(self.sipmVChannels-2):
                voltages1.append(self.data[2*(self.radFlagChannels+ x), -10:])
            for x in range(self.sipmVChannels-2, self.sipmVChannels):
                voltages2.append(self.data[2*(self.radFlagChannels + x), -10:])
            voltages1 = np.concatenate(voltages1)
            mean1 = np.round(np.mean(voltages1), 3)
            sd1 = np.round(np.std(voltages1), 3)
            voltages2 = np.concatenate(voltages2)
            mean2 = np.round(np.mean(voltages2), 3)
            sd2 = np.round(np.std(voltages2), 3)

            if(graphNumber == 4):  # Make a popout graph
                title = "SIPM Voltage Distribution"
                ylabel, xlabel = "Counts", "Voltage (V)"
                data = self.getVoltageData()
                channel, xlim, ylim = 2, [0, 0], [0, 0],
                graphType, source = "hist", self
                Graph(title, xlabel, ylabel, channel, data, xlim, ylim,
                      graphType, source)
            else:  # Make the graph on the main GUI
                self.ax[graphNumber].clear()
                self.ax[graphNumber].hist(voltages1, bins=10,
                                          label="Channels 1-15",
                                          histtype="step", color="r")
                self.ax[graphNumber].axvline(mean1-sd1,
                                             label="Channels 1-15 Mean +/- sd",
                                             color="r", linestyle="--")
                self.ax[graphNumber].axvline(mean1+sd1, color="r",
                                             linestyle="--")
                self.ax[graphNumber].hist(voltages2, bins=10,
                                          label="Channels 16-17",
                                          histtype="step", color="k")
                self.ax[graphNumber].axvline(mean2-sd2, color="k",
                                             label="Channels 16-17 Mean +/- sd",
                                             linestyle="--")
                self.ax[graphNumber].axvline(mean2+sd2, color="k",
                                             linestyle="--")
                self.ax[graphNumber].set_ylim(0, 25)

                self.ax[graphNumber].legend()
                self.ax[graphNumber].set_xlabel("Voltage (V)")
                self.ax[graphNumber].set_ylabel("Counts")
                self.ax[graphNumber].set_title("SIPM Voltage Distribution")
                self.canvas.draw()

    def getRecentData(self):
        """
        Utility function that returns the most recent data collected from the
        DMM.
        """
        return(self.data[:, -1])

    def getChannelData(self, channel):
        """
        Utility function that returns all the data for a given channel
        """
        return(self.data[channel, :])

    def getVoltageData(self):
        """
        Utility function for the histogram graphs. This returns a list with
        the SIPM output voltages, mean, and sd from one set of the channels,
        and the same values for the other set of channels as well.
        """
        voltages1 = []
        voltages2 = []
        for x in range(self.sipmVChannelsCategory1):
            voltages1.append(self.data[2*(self.radFlagChannels+ x), -10:])
        for x in range(self.sipmVChannelsCategory1, self.sipmVChannels):
            voltages2.append(self.data[2*(self.radFlagChannels + x), -10:])
            
            
            
            
        voltages1 = np.concatenate(voltages1)
        mean1 = np.round(np.mean(voltages1), 3)
        sd1 = np.round(np.std(voltages1), 3)
        voltages2 = np.concatenate(voltages2)
        mean2 = np.round(np.mean(voltages2), 3)
        sd2 = np.round(np.std(voltages2), 3)
        return([voltages1, mean1, sd1, voltages2, mean2, sd2])

    def giveGraphResults(self, measurement, value):
        """
        A function used by the graph form to return its results to this main
        object. It interprets the results and then creates the appropriate pop
        out graph.

        Parameters
        ----------
        measurement : String
            The graph to create
        value : String list
            A list with the channel for the graph and the past hours to graph.

        Returns
        -------
        None.
        """

        # Determine th channel
        channel = value[0]
        if(measurement == "Voltage Histogram"):
            channel = "0"
        pastHours = value[1]
        # Assuming a channel was given, create the appropriate graph
        if(len(channel) > 0):
            channel = int(channel)
            if(len(pastHours) > 0):
                pastHours = float(pastHours)
            else:
                pastHours = -1
            if(measurement == "Current"):
                self.currentGraph(channel, pastHours, 4)
            elif(measurement == "SIPM Output Voltage"):
                self.voltageGraph(channel, pastHours, 4)
            elif(measurement == "Temperature"):
                self.temperatureGraph(channel, pastHours, 4)
            elif(measurement == "Radiation"):
                self.radiationFlagGraph(channel, pastHours, 4)
            elif(measurement == "Power Supply Voltage"):
                self.powerSupplyGraph(channel, pastHours, 4)
            elif(measurement == "Voltage Histogram"):
                self.voltageHist(4)

    def giveBiasResults(self, value):
        """
        A function used by the bias form to return its results to this main
        object. It interprets the results and then adjuts the bias voltages
        from the power supplies appropriately.

        Parameters
        ----------
        value : String list
            A list with the channel for the bias adjustemtn and the new bias
            value

        Returns
        -------
        None.
        """

        # Check for a channel
        channel = value[0]
        if(len(channel) > 0):
            channel = int(channel)
        else:
            channel = channel - 1
        
        
         # Check for a new bias voltage
        val = value[1]
        if(len(val) > 0):
            val = float(val)
        else:
            channel = channel - 1
        
        if(channel in [1,2]):
            # Figure out the unit number
            unit = 1
            self.setPS(unit, channel, val)
        else:
        	print("You gave an invalid bias channel, it must be between 1 and 2 inclusive")
   
    def giveBiasScanResults(self, value):
        """
        A function used by the bias form to return its results to this main
        object. It interprets the results and then adjuts the bias voltages
        from the power supplies appropriately.

        Parameters
        ----------
        value : String list
            A list with the channel for the bias adjustemtn and the new bias
            value

        Returns
        -------
        None.
        """
        
        """
        ["Cycles", "Bias Channel", "LED channel", "Temperature Acclimation Time (minutes)",
         "Pedestal Time (minutes)", "LED Warmup Time (minutes)", "Bias Scan step (minutes)",
         "Nominal Bias (V)", "Nominal LED (V)", "Start Scan Below Nominal (V)",
         "Run Scan Above Nominal (V)", "Scan Step Size (V)"]
        """

        try:
        
            minuteScale = 1000*60    
        
            cycles = int(value[0])
            biasChannel = int(value[1])
            ledChannel = int(value[2])
            sipmWarmupTime = float(value[3])*minuteScale
            pedestalTime = float(value[4])*minuteScale
            ledWarmUpTime = float(value[5])*minuteScale
            scanStepTime = float(value[6])*minuteScale
            nominalBias = float(value[7])
            nominalLED = float(value[8])
            scanLower = float(value[9])
            scanUpper = float(value[10])
            scanStepVoltage = float(value[11])        
    
            if(biasChannel<1 or biasChannel>2 or ledChannel<1 or ledChannel>2):
                print("Error, bias and led channels must be between 1 and 2 inclusive", value)
                return()
            
            if(sipmWarmupTime<0 or pedestalTime<0 or ledWarmUpTime<0 or scanStepTime<0 or nominalBias<0 or nominalLED<0 or scanLower<0 or scanUpper<0 or scanStepVoltage<0):
                print("Error, all times and voltages must be greater than or equal to 0", value)
                
                
            if(scanStepVoltage==0 and scanUpper+scanLower>0):
                print("Error, bias scan step must be greater than 0 if the scan range is greater than 0", value)
                
            if(scanLower>nominalBias):
                print("Error, bottom of bias scan must not be below 0 V", value)
                
            biasUnit = 1
            ledUnit = 1
    
            for cylce in range(cycles):
                
                # Turn LED off
                waitTime = 0
                self.taskQueue.append(Task(self.setPS, [ledUnit, ledChannel, 0], waitTime, "Set LED voltage to 0")) 
                
                
                # Set nominal bias
                waitTime = sipmWarmupTime
                self.taskQueue.append(Task(self.setPS, [biasUnit, biasChannel, nominalBias], waitTime, "Set Bias voltage to "+str(nominalBias))) 
                
                # Measure pedestal
                waitTime = pedestalTime
                self.taskQueue.append(Task(self.setPS, [biasUnit, biasChannel, 0], waitTime, "Set LED voltage to 0")) # Measure pedestal
                
                
                # Turn LED to nominal
                waitTime = ledWarmUpTime
                self.taskQueue.append(Task(self.setPS, [ledUnit, ledChannel, nominalLED], waitTime, "Set LED voltage to 0")) 
                
                # Bias scan upwards
                for bias in np.arange(nominalBias-scanLower, nominalBias+scanUpper, scanStepVoltage):
                    # Measure pedestal
                    waitTime = scanStepTime
                   
                    self.taskQueue.append(Task(self.setPS, [biasUnit, biasChannel, bias], waitTime, "Set Bias voltage to "+str(bias))) # Measure pedestal
                    
                # Bias scan down
                for bias in np.arange(nominalBias+scanUpper, nominalBias-scanLower, -scanStepVoltage):
                    # Measure pedestal
                    waitTime = scanStepTime
                    self.taskQueue.append(Task(self.setPS, [biasUnit, biasChannel, bias], waitTime, "Set Bias voltage to "+str(bias))) # Measure pedestal
                    
                # Hit lower bound again
                waitTime = scanStepTime
                self.taskQueue.append(Task(self.setPS, [biasUnit, biasChannel, nominalBias-scanLower], waitTime, "Set Bias voltage to "+str(nominalBias-scanLower))) # Measure pedestal
                
                # Turn LED off
                waitTime = 0
                self.taskQueue.append(Task(self.setPS, [ledUnit, ledChannel, 0], waitTime, "Set LED voltage to 0")) 
                
                # Set nominal bias
                waitTime = 0
                self.taskQueue.append(Task(self.setPS, [biasUnit, biasChannel, nominalBias], waitTime, "Set Bias voltage to "+str(nominalBias))) 
                
            if(not self.queueRunning):
                self.executeTask()
        
                
        except Exception as error:
            print("Error in input:", value)
            print("Error:", error)

    def giveNote(self, note):
        """
        Saves a note given by the user to the log file with a time stamp

        Parameters
        ----------
        note : String
            Note given by user

        Returns
        -------
        None.

        """
        timeVal = time.time()
        message = 'UserNote|' + note + '|SUCCESS|' + str(timeVal)+"\n"
        self.log_file.write(message)

    def on_closing(self):
        """
        Action to take when a user attempts to close the program. This verifies
        their intention after informing them that this will stop data
        acquisiton. If the code is executed it saves all the data to a file
        and closes all the VISA connections aftert stopping the DMM scan.

        Returns
        -------
        None.

        """

        if tk.messagebox.askokcancel("Quit", "Are you sure you want to quit?" +
                                     " This will stop data acquisition."):

            # Save a full copy of the data
            np.save("full_"+str(int(time.time()))+".npy", self.data)
            np.save("fullRigol_"+str(int(time.time()))+".npy", self.rigolData)
            self.log_file.close()
            
            # Stop the scans and close VISA connections
            self.dmm.write(":ABORt")
            self.dmm.close()
            
            self.ps1.write("OUTP CH1, OFF")
            self.ps1.write("OUTP CH2, OFF")
            self.ps1.close()
            
            # Destroy the GUI
            self.quit()
            self.destroy()

class Form(tk.Toplevel):
    """
    A utility object used to obtain information from the user with a dropdown
    box.
    """
    def __init__(self, root, options, parameters, formType):
        """
        Create the Form

        Parameters
        ----------
        root : MainUI
            MainUI object that created the Form object
        options: String list
            options for the dropdown box
        paramters: String list
            Prompts for user fill in data
        formType: String
            Either graph or bias, denoting which form this is

        Returns
        -------
        None.

        """
        super().__init__()
        self.formType = formType
        self.root = root
        # Dropdown menu options
        self.options = options
        # datatype of menu text
        self.clicked = tk.StringVar()

        # initial menu text
        self.clicked.set(options[0])

        self.labels = []
        self.values = []

        # Create Dropdown menu
        self.measurementLabel = tk.Label(self, text="Measurement")
        self.measurementLabel.grid(row=0, column=0)
        self.measurementValue = tk.OptionMenu(self, self.clicked,
                                              *self.options)
        self.measurementValue.grid(row=0, column=1)
        # Create the short user entered boxes.
        for x in range(len(parameters)):
            self.labels.append(tk.Label(self, text=parameters[x]))
            self.labels[-1].grid(row=1+x, column=0, columnspan=1,
                                 sticky="NESW")

            self.values.append(tk.Entry(self))
            self.values[-1].grid(row=1+x, column=1, columnspan=1,
                                 sticky="NESW")

        # Create button, it will change label text
        self.button = tk.Button(self, text="Submit", command=self.submit)
        self.button.grid(row=1+len(parameters), columnspan=2, sticky="NESW")

    # Change the label text
    def submit(self):
        """
        Gets the results from the form, returns them to the main UI object,
        then deletes this object.
        """
        # Get the drop down
        measurement = self.clicked.get()

        # Get the user inputs and sanitize them
        values = []
        for x in range(len(self.values)):
            values.append(self.sanitize(self.values[x].get()))

        # Return the results to the main UI object
        if(self.formType == "graph"):
            self.root.giveGraphResults(measurement, values)
        elif(self.formType == "bias"):
            self.root.giveBiasResults(values)
        elif(self.formType == "biasScan"):
            self.root.giveBiasScanResults(values)
        self.destroy()

    def sanitize(self, val):
        """
        Parameters
        ----------
        val : string
            An input string which is supposed to contain a number

        Returns
        -------
        number : string
            The input string with all none numerical characters removed

        """

        # Only keep numbers and periods in the input string
        temp = [x for x in val]
        number = ""
        for val in temp:
            if(val.isdigit() or val == "." or val =="-"):
                number += val
        return(number)


class Note(tk.Toplevel):
    def __init__(self, root):
        """
        A simple note object prompts the user for a note, returns it to the GUI
        that created the object, and then destroys itself.

        Parameters
        ----------
        root : tkinter object
            A pointer to the main GUI object

        Returns
        -------
        None.

        """
        super().__init__()

        self.root = root

        # Creates a prompt, text field, and submission button
        self.label = tk.Label(self, text="Type a note below:")
        self.label.grid(row=0, column=0)
        self.textInput = tk.Text(self, height=20, width=35)
        self.textInput.grid(row=1, column=0)
        self.button = tk.Button(self, text="Submit", command=self.submit)
        self.button.grid(row=2, column=0, sticky="NESW")

    def submit(self):
        """
        Get the data the user entered into the note field and return it
        to the main GUI through a giveNote function in the main GUI.

        Returns
        -------
        None.

        """

        # Get the note and return it
        note = self.textInput.get("1.0", "end-1c")
        self.root.giveNote(note)

        # Destroy this window
        self.destroy()
        
        
class Task(object):
    """
    A stand in object that currently does nothing. This may later have
    functionality added to automatically make adjustements to the experiment
    based on data
    """
    def __init__(self, function, arguments, time, description):
        self.function = function
        self.arguments = arguments
        self.time = int(time)
        self.description = description + " for " + str(self.time/(60*1000)) + " minutes"

    def execute(self):
        self.function(*self.arguments)
        
    def getDuration(self):
        return(self.time)
        
    def getDescription(self):
        return(self.description)


class GridData(tk.Toplevel):
    """
    An object used to show the numerical values of the most recent
    measurements.
    """
    def __init__(self, source, radFlagChannels, sipmVChannels, biasVoltageChannels, temperatureChannels, currentChannels):
        """
        Parameters
        ----------
        source : MainUI
            MainUI object that created the GridData object

        Returns
        -------
        None.

        """
        super().__init__()

        totalChannels = radFlagChannels + sipmVChannels + biasVoltageChannels + temperatureChannels + currentChannels


        if(totalChannels%2==1):
            self.oddChannels = True
        else:
            self.oddChannels = False
            
        
        if(self.oddChannels):
            self.rows = (totalChannels+1)//2
        else:
            self.rows = (totalChannels)//2

        # Configure the GUI
        self.dataSource = source

        self.root = self

        self.dataView = ttk.Treeview(self.root, height=(totalChannels+1)//2)
        
        self.mainFontSize = source.mainFontSize

        self.style = ttk.Style()
        self.style.configure("Treeview.Heading", font=(None, self.mainFontSize))
        self.style.configure("Treeview", font=(None, self.mainFontSize), rowheight=source.allRowsHeight)

        self.dataView['columns'] = ('measurement1', 'value1', 'blank',
                                    'measurement2', 'value2')
        self.measurementLabels = []

        # Add row labels
        for x in range(radFlagChannels):
            self.measurementLabels.append("Radiation Flags "+str(x+1)+" (V)")
        for x in range(sipmVChannels):
            self.measurementLabels.append("SIPM Voltage Out "+str(x+1)+" (mV)")
        for x in range(biasVoltageChannels):
            self.measurementLabels.append("Bias Voltages "+str(x+1)+" (V)")
        for x in range(temperatureChannels):
            self.measurementLabels.append("Temperature "+str(x+1)+" (C)")
        for x in range(currentChannels):
            self.measurementLabels.append("Current Out "+str(x+1)+" (micro A)")


        # Add column labels
        self.dataView.column("#0", width=0, stretch="NO")
        self.dataView.column("measurement1", anchor="center", width=source.measurementColumnWidth)
        self.dataView.column("value1", anchor="center", width=source.valueColumnWidth)
        self.dataView.column("blank", anchor="center", width=source.gapColumnWidth)
        self.dataView.column("measurement2", anchor="center", width=source.measurementColumnWidth)
        self.dataView.column("value2", anchor="center", width=source.valueColumnWidth)

        self.dataView.heading("#0", text="", anchor="center")
        self.dataView.heading("measurement1", text="Measurement",
                              anchor="center")
        self.dataView.heading("value1", text="Value", anchor="center")
        self.dataView.heading("blank", text="", anchor="center")
        self.dataView.heading("measurement2", text="Measurement",
                              anchor="center")
        self.dataView.heading("value2", text="Value", anchor="center")

        # Put in temporary data
        for x in range(self.rows-1):
            self.dataView.insert(parent='', index='end', iid=x, text='',
                                 values=(self.measurementLabels[x],
                                         np.round(np.random.uniform(0, 1), 3),
                                         "", self.measurementLabels[x+self.rows],
                                         np.round(np.random.uniform(0, 1), 3)))
        if(self.oddChannels):
            self.dataView.insert(parent='', index='end', iid=self.rows-1,
                                 text='',
                                 values=(self.measurementLabels[self.rows-1],
                                         np.round(np.random.uniform(0, 1), 3),
                                         "", "", ""))
        else:
            self.dataView.insert(parent='', index='end', iid=self.rows-1,
                                 text='',
                                 values=(self.measurementLabels[self.rows-1],
                                         np.round(np.random.uniform(0, 1), 3),
                                         "",
                                         self.measurementLabels[2*self.rows-1],
                                         np.round(np.random.uniform(0, 1), 3)))

        # Format the GUI and schedule a data update
        self.dataView.pack()
        self.dataView.after(2000, self.updateData)

    def updateData(self):
        """
        Gets fresh data from the the main GUI object and updates the displayed
        data.
        """
        measurements = self.dataSource.getRecentData()
        measurements = np.round(measurements, 3)
        for x in range(self.rows-1):
            self.dataView.item(x, values=(self.measurementLabels[x],
                                          measurements[x*2], "",
                                          self.measurementLabels[x+self.rows],
                                          measurements[x*2+self.rows*2]))
        if(self.oddChannels):
            self.dataView.item(self.rows-1, values=(self.measurementLabels[self.rows-1],
                                         measurements[(self.rows-1)*2],
                                         "", "", ""))
        else:
            self.dataView.item(self.rows-1, values=(self.measurementLabels[self.rows-1],
                                                    measurements[(self.rows-1)*2],
                                                    "",
                                                    self.measurementLabels[self.rows*2-1],
                                                    measurements[self.rows*4-2]))
        self.update()

        self.dataView.after(2000, self.updateData)


class Graph(tk.Toplevel):
    """
    This object handles a single popout graph. It periodically querries the
    main UI object for more recent data and updates the graph.
    """
    def __init__(self, title, xlabel, ylabel, channel, data, xlim, ylim,
                 graphType, source):
        """
        Parameters
        ----------
        title : String
            The title for the graph
        xlabel : String
            The label for the x axis of the graph
        ylabel : String
            The label for the y axis of the graph
        channel : Int
            The channel this graph corresponds to
        data : Float list
            Data for graphing
        xlim : Float list
            List with the min and max x values
        ylim : Float list
            List with the min and max y values
        graphType : String
            Details what kind of graph this is
        source : MainUI
            MainUI object that created the GridData object

        Returns
        -------
        None.
        """

        # Graph settings
        super().__init__()
        #font = {'size': 14}
        #matplotlib.rc('font', **font)
        #matplotlib.rc('xtick', labelsize=10)
        #matplotlib.rc('ytick', labelsize=14)

        # GUI settings
        self.root = self
        self.root.wm_title(title)
        self.channel = channel
        self.source = source
        self.fig = Figure(figsize=(8, 6), dpi=100)
        self.figPlot = self.fig.add_subplot(111)
        self.graphType = graphType

        # Graph info
        self.graphTitle = title
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.ylim = ylim
        self.xlim = xlim

        # Graph and Hist are different, make the acutal graphs
        if(self.graphType == "hist"):
            self.figPlot.hist(data[0], bins=10, label="Channels 1-10",
                              histtype="step", color="r")
            self.figPlot.axvline(data[1]-data[2],
                                 label="Channels 1-15 Mean +/- sd",
                                 color="r", linestyle="--")
            self.figPlot.axvline(data[1]+data[2], color="r", linestyle="--")
            self.figPlot.hist(data[3], bins=10, label="Channels 11-20",
                              histtype="step", color="k")
            self.figPlot.axvline(data[4]-data[5], color="k",
                                 label="Channels 16-17 Mean +/- sd",
                                 linestyle="--")
            self.figPlot.axvline(data[4]+data[5], color="k", linestyle="--")
            self.figPlot.set_title(title)
            self.figPlot.set_xlabel(xlabel)
            self.figPlot.set_ylabel(ylabel)
        else:
            self.figPlot.scatter(data[0], data[1])
            if(len(self.xlim) == 2):
                self.figPlot.set_xlim(xlim[0], xlim[1])
            if(len(self.ylim) == 2):
                self.figPlot.set_ylim(ylim[0], ylim[1])
            self.figPlot.set_title(title)
            self.figPlot.set_xlabel(xlabel)
            self.figPlot.set_ylabel(ylabel)

        # Put the graph on the canvas and GUI
        self.canvas = FigureCanvasTkAgg(self.fig, master=self.root)
        self.fig.tight_layout()
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side="top", fill="both", expand=1)

        toolbar = NavigationToolbar2Tk(self.canvas, self.root)
        toolbar.update()
        self.canvas.get_tk_widget().pack(side="top", fill="both", expand=1)

        self.canvas.mpl_connect("key_press_event", self.onKeyPress)

        self.root.after(2000, self.updateData)

    def updateData(self):
        """
        Obtains new data for the graph from the source object and updates the
        graph
        """

        # Clear old graph
        self.figPlot.clear()

        # Make histogram if histogram
        if(self.graphType == "hist"):
            data = self.source.getVoltageData()
            self.figPlot.hist(data[0], bins=10, label="Channels 1-10",
                              histtype="step", color="r")
            self.figPlot.axvline(data[1]-data[2],
                                 label="Channels 1-2 Mean +/- sd",
                                 color="r", linestyle="--")
            self.figPlot.axvline(data[1]+data[2], color="r", linestyle="--")
            self.figPlot.hist(data[3], bins=10, label="Channels 11-20",
                              histtype="step", color="k")
            self.figPlot.axvline(data[4]-data[5], color="k",
                                 label="Channels 3-4 Mean +/- sd",
                                 linestyle="--")
            self.figPlot.axvline(data[4]+data[5], color="k", linestyle="--")
            self.figPlot.set_title(self.graphTitle)
            self.figPlot.set_xlabel(self.xlabel)
            self.figPlot.set_ylabel(self.ylabel)
            self.figPlot.legend()
        else:  # Make other kind of graph
            data = []
            data = [self.source.getChannelData(self.channel+1),
                    self.source.getChannelData(self.channel)]
            self.figPlot.scatter(data[0], data[1])
            # Adjust plot limits, keep previous xmin
            if(len(self.xlim) == 2):
                self.figPlot.set_xlim(self.xlim[0], max(data[0]*1.01))
            if(len(self.ylim) == 2):
                yLimMin = min(min(data[1][data[0]>self.xlim[0]])*0.99, min(data[1][data[0]>self.xlim[0]])*1.01)
                yLimMax = max(max(data[1][data[0]>self.xlim[0]])*1.01,max(data[1][data[0]>self.xlim[0]])*0.99)
                self.figPlot.set_ylim(yLimMin,yLimMax)
            self.figPlot.set_title(self.graphTitle)
            self.figPlot.set_xlabel(self.xlabel)
            self.figPlot.set_ylabel(self.ylabel)
        self.fig.tight_layout()
        self.canvas.draw()
        self.after(2000, self.updateData)

    def onKeyPress(self, event):
        """
        Handles a key press on the Graph GUI

        Parameters:
            event : the event from the GUI
        """
        print("you pressed {}".format(event.key))
        key_press_handler(event, self.canvas, self.toolbar)

    def on_closing(self):
        """
        Instructions for closing the GUI.
        """
        self.destroy()
        self.quit()


if(__name__ == "__main__"):
    ui = MainUI()
    ui.mainloop()
