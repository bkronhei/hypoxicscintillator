import pyvisa
import matplotlib
import os
import time
import traceback

import numpy as np
import tkinter as tk

from tkinter import ttk
from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg, NavigationToolbar2Tk)
from matplotlib.figure import Figure
from matplotlib.backend_bases import key_press_handler


class MainUI(tk.Tk):
    def __init__(self):
        super().__init__()

        # system time in seconds of the dmm and computer at start
        self.dmmStartTime = 0
        self.computerStartTime = 0

        # Time interval at which to save data
        self.saveTime = 120

        # Time interval at which to save data
        self.fullSaveTime = 3600*24  # (1 day)

        # Time interval at which to query DMM
        self.queryTime = 5

        # Number of channels of data read from the DMM
        self.channelNumber = 11

        # What to do when the window is closed
        self.protocol("WM_DELETE_WINDOW", self.on_closing)

        # Make directory in which to save the data files
        directory = os.getcwd()
        if(not os.path.exists(directory+"/tempFiles")):
            os.makedirs(directory+"/tempFiles")

        # Log File
        self.log_file = open('log'+str(int(time.time()))+'.txt', 'a')

        # UI title
        self.wm_title("Hypoxic Scintilator Test")

        self.voltChannels = [101, 102, 103, 104, 105, 106, 107]
        self.resistanceChannels = [108, 109, 110]
        self.currentChannels = [121]
        self.temperatureIndices = [7,8,9]
        self.cableResistances = [1,1,1]

        # Font size is 14
        font = {'size': 14}
        matplotlib.rc('font', **font)
        matplotlib.rc('xtick', labelsize=14)
        matplotlib.rc('ytick', labelsize=14)

        # Number of times data has been taken from the DMM
        self.counter = 0

        # First measurement to save next time data is written to file
        self.saveIndex = 0
        self.data = []
        self.rigolSaveIndex = 0
        self.rigolData = []

        # Create the area in which the four rotating graphs are displayed
        self.fig = matplotlib.figure.Figure(figsize=(20, 10))
        self.ax = [self.fig.add_subplot(221), self.fig.add_subplot(222),
                   self.fig.add_subplot(223), self.fig.add_subplot(224)]
        self.canvas = FigureCanvasTkAgg(self.fig, master=self)
        self.canvas.draw()
        self.canvas.get_tk_widget().grid(row=0, column=0, columnspan=4)

        # Create buttons on the GUI
        gridButton = tk.Button(master=self, text="Show Grid ",
                               command=self.gridFunction, font=(None, 14))
        graphButton = tk.Button(master=self, text="New Graph ",
                                command=self.graphFunction, font=(None, 14))
        updateOffsetButton = tk.Button(master=self,
                                       text="Update Bias Voltage ",
                                       command=self.biasFunction,
                                       font=(None, 14))
        noteButton = tk.Button(master=self,
                               text="Add note ",
                               command=self.noteFunction,
                               font=(None, 14))

        # Assign locations to the buttons
        gridButton.grid(row=2, column=0, columnspan=1, sticky="NESW")
        graphButton.grid(row=2, column=1, columnspan=1, sticky="NESW")
        updateOffsetButton.grid(row=2, column=2, columnspan=1, sticky="NESW")
        noteButton.grid(row=2, column=3, columnspan=1, sticky="NESW")

        # Store the DMM ranges and power supply voltages in case of loss
        # of connection
        self.ranges = {}
        self.psValues = {"PS1C1": .1, "PS1C2": .1, "PS2C1": .1, "PS2C2": .1}

        # Contains the order for the graphs to appear
        self.graphCycle = []
        self.graphCycle.append(["V1", "V2", "V3"])
        self.graphCycle.append(["T1", "T2", "T3"])
        self.graphCycle.append(["R1", "R2", "H1"])
        self.graphCycle.append(["P1", "P2", "C1"])
        self.graphCycleLocation = 0

        # Setup and start running the scan
        self.initializeScan()

        # Setup triggers for automatic data based code execution
        self.triggers = Trigger(self)

        # Create the graphs after 5 seconds to allow data to accumulate
        self.after(10000, self.rotateGraphs)

    def initializeScan(self):
        """
        Creates a connection with the DMM and power supplies, sets the
        measurement ranges to their proper values, creates a scan, and then
        runs the scan. This is used at initialization of the object and any
        time a connection is dropped.

        Returns
        -------
        None.

        """

        # Determine the buffer size
        scansPerBuffer = 10000  # This value should maybe be tweeked
        self.bufferSize = scansPerBuffer * self.channelNumber
        self.previousLocation = 1  # Starting point to take new data

        self.rm = pyvisa.ResourceManager()

        # Verify that theer are three visa connections. If not, launch recovery
        # until there are three.
        allAddresses = self.rm.list_resources()
        print(allAddresses)
        if(len(allAddresses) != 3):
            timeVal = str(int(time.time()))
            self.log_file.write('MissingVISAAdress|0|FAIL|' + timeVal + "\n")
            self.recovery()
            # Recovery calls this function again, so no further action is
            # needed here if recovery is launched
        else:
            # Determine VISA addresses for all three devices
            for val in allAddresses:
                if("DP8G231800056" in val):
                    self.visaAddressPS1 = val
                elif("DP8G231800060" in val):
                    self.visaAddressPS2 = val
                else:
                    self.visaAddressDMM = val

            # Open up all three VISA connections and reset the devices
            self.dmm = self.rm.open_resource(self.visaAddressDMM)
            self.dmm.write("*RST")
            self.ps1 = self.rm.open_resource(self.visaAddressPS1)
            self.ps1.write("*RST")
            self.ps2 = self.rm.open_resource(self.visaAddressPS2)
            self.ps2.write("*RST")

            # Set the power supply units to their proper voltages
            self.setPS(1, 1, self.psValues["PS1C1"])
            self.setPS(1, 2, self.psValues["PS1C2"])
            self.setPS(2, 1, self.psValues["PS2C1"])
            self.setPS(2, 2, self.psValues["PS2C2"])

            # Turn on the outputs of the power supply units
            self.ps1.write("OUTP CH1, ON")
            self.ps1.write("OUTP CH2, ON")
            self.ps2.write("OUTP CH1, ON")
            self.ps2.write("OUTP CH2, ON")

            # Configure the channels of the DMM
            self.dmm.write(':TRAC:POIN '+str(self.bufferSize)+',"defbuffer1"')
            self.dmm.write('TRACe:FILL:MODE CONT, "defbuffer1"')
            self.dmm.write(':ROUT:SCAN:BUFF "defbuffer1"')
            self.dmm.write("FUNC 'VOLT:DC', (@101:107)")
            self.dmm.write("VOLT:DC:RANG AUTO, (@101:107)")
            self.dmm.write("VOLT:DC:NPLC 1, (@101:107)")
            self.dmm.write("FUNC 'RES', (@108:110)")
            self.dmm.write("RES:RANG 100000, (@108:110)")
            self.dmm.write("FUNC 'CURR:DC', (@121)")
            self.dmm.write("CURR:DC:RANG AUTO, (@121)")

            # Set the ranges for the DMM measurements
            for key in self.ranges.keys():
                self.setRange(int(key), self.ranges[key])

            # Get the dmm system time
            dmmTime = self.dmm.query("SYST:TIME?")
            if(len(dmmTime) > 0):
                dmmTime = dmmTime[:-1]
            # Get the computer system time
            timeVal = str(int(time.time()))
            # Log the time synch information
            if(len(dmmTime) > 0):
                message = "TimeSynch|" + dmmTime + "|SUCCESS|" + timeVal+"\n"
                self.log_file.write(message)
            else:
                message = "TimeSynch|" + dmmTime + "|FAIL|" + timeVal+"\n"
                self.log_file.write(message)

            # Determine the offset between the DMM system time and experiment
            # start time
            dmmTime = float(dmmTime)
            timeVal = float(timeVal)
            if(self.dmmStartTime == 0):
                self.computerStartTime = timeVal
                self.dmmStartTime = dmmTime
                self.startTimeOffset = timeVal - dmmTime
            currentTimeOffset = timeVal - dmmTime
            self.timeShift = currentTimeOffset - self.dmmStartTime
            self.timeShift = self.timeShift - self.startTimeOffset

            # Create and run a scan
            self.dmm.write("ROUT:SCAN (@101:110,121)")
            self.dmm.write("ROUT:SCAN:COUN:SCAN 0")
            self.dmm.write("ROUT:SCAN:INT 3")
            self.dmm.write("TRACe:CLEar")
            self.dmm.write("INIT")

            # Get data from the DMM, this will be called every 2 seconds
            self.after(1000*self.queryTime, self.updateData)

    def updateData(self):
        """
        Loads data from the dmm and uses it to update the data stored in RAM.
        Saves a portion of data to the drive every n queries where
        n = saveTime//queryTime.
        Saves all the data to drive every n queries where
        n = saveTime//fullSaveTime
        If the DMM cannot be read, launches recovery

        Returns
        -------
        None.

        """

        # Query the DMM
        tmpBuff, success = self.readDMM()
        # Assuming the DMM waas sucessfully read,
        if(success):
            # Update the data in RAM
            if(len(self.data) == 0):
                self.data = tmpBuff
            else:
                self.data = np.concatenate([self.data, tmpBuff], axis=1)

            # Determine whether the data should be written to disk
            if((self.counter+1) % (self.saveTime//self.queryTime) == 0):
                np.save("tempFiles/temp_"+str(int(time.time()))+".npy",
                        self.data[:, self.saveIndex:])
                self.saveIndex = self.data.shape[1]
            if((self.counter+1) % (self.fullSaveTime//self.queryTime) == 0):
                np.save("full_"+str(int(time.time()))+".npy", self.data)

            # Update counter of DMM queries
            self.counter += 1

            # Schedule the next DMM query
            self.after(1000*self.queryTime, self.updateData)
        else:  # If it wasn't, launch into recovery mode and log it
            message = 'DMMReadFail|0|FAIL|' + str(int(time.time())) + "\n"
            self.log_file.write(message)
            self.recovery()

        # Query the Rigol
        tmpBuff, success = self.readRigol()

        # Assuming the DMM waas sucessfully read,
        if(success):
            tmpBuff = np.reshape(tmpBuff, (13, 1))
            tmpBuff = np.float32(tmpBuff)
            # Update the data in RAM
            if(len(self.rigolData) == 0):
                self.rigolData = tmpBuff
            else:
                self.rigolData = np.concatenate([self.rigolData, tmpBuff],
                                                axis=1)
            # Determine whether the data should be written to disk
            if((self.counter+1) % (self.saveTime//self.queryTime) == 0):
                np.save("tempFiles/tempRigol_"+str(int(time.time()))+".npy",
                        self.rigolData[:, self.rigolSaveIndex:])
                self.rigolSaveIndex = self.rigolData.shape[1]
            if((self.counter+1) % (self.fullSaveTime//self.queryTime) == 0):
                np.save("fullRigol_"+str(int(time.time()))+".npy",
                        self.rigolData)

        else:  # If it wasn't, launch into recovery mode and log it
            message = 'RigolReadFail|0|FAIL|' + str(int(time.time())) + "\n"
            self.log_file.write(message)
            self.recovery()

    def readDMM(self):
        """
        Querries the DMM to get the most recent data measured.

        Returns
        -------
        data: Float32 Array
            An array of double values containg the most recent data extracted
            from the DMM
        sucees: Boolean
            A single Boolean denoting whether data was sucessfully extracted
            from the DMM

        """

        # Default is failure to read DMM
        success = False

        # Catch errors when communicating with the DMM
        try:
            # Get the number of completed scans
            lastIndex = self.dmm.query("ROUTe:SCAN:STATe?")
            lastIndex = lastIndex.split(";")[1]

            # Convert to number of measurements
            lastIndex = int(lastIndex)*self.channelNumber

            # Determine index within the buffer
            lastIndex = lastIndex % self.bufferSize
            if(lastIndex == 0):
                lastIndex = self.bufferSize
            if(lastIndex < self.previousLocation):
                lastIndex = self.bufferSize
            # SCPI command to get the data from the DMM
            command = 'TRACe:DATA? ' + str(self.previousLocation) + ', '
            command += str(lastIndex) + ', "defbuffer1", READ, CHAN, SEC'

            # Convert the data taken from the DMM to a usable shape
            tmpBuff = self.dmm.query(command)
            tmpBuff = tmpBuff[:-1]
            tmpBuff = np.array(tmpBuff.split(","))
            tmpBuff = np.reshape(tmpBuff, (len(tmpBuff)//3, 3))
            tmpBuff = np.float64(tmpBuff)
            # Apply the time shift
            tmpBuff[:, 2] = tmpBuff[:, 2] + self.timeShift
            # Note location of last data extraction
            self.previousLocation = lastIndex+1
            self.previousLocation = self.previousLocation % self.bufferSize
            # Remove unncesary information
            tmpBuff = np.concatenate([tmpBuff[:, 0:1], tmpBuff[:, 2:3]],
                                     axis=1)

            # Get the data into the proper shape for the rest of the program
            data = []
            dataChunkSize = tmpBuff.shape[0]//(self.channelNumber)
            for x in range(dataChunkSize):
                index1 = x*self.channelNumber
                index2 = (x+1)*self.channelNumber
                dataSize = self.channelNumber*2
                dataChunk = tmpBuff[index1:index2, :].reshape(dataSize, 1)
                data.append(dataChunk)
            data = np.concatenate(data, axis=1)
            for (index, res) in zip(self.temperatureIndices, self.cableResistances):
                data[2*index,:] = self.calculateTemperature(data[2*index,:]-res)
            success = True
        except:  # Something went wrong
            traceback.print_exc()
            data = np.float32(np.array([1]))
        return(data, success)

    def calculateTemperature(self, resistance):
        """
        Converts a resistance to a temperature.

        Parameters
        ----------
        resistance : Float32
            The resistance to convert

        Returns
        -------
        temeperature : Float32
            The temperature corresponding to the given resistance

        """

        a = -246.2150271682803
        b = 0.023654686381224133
        c = 9.668176211494519e-08
        d = 2.7

        resistance = resistance - d
        temperature = a + b*resistance + c*(resistance**2)
        return(temperature)

    def readRigol(self):
        """
        Querries the Rigol to get the output voltages, current, and power.

        Returns
        -------
        data: Float Array
            An array of values containg the most recent data extracted
            from the Rigols
        sucees: Boolean
            A single Boolean denoting whether data was sucessfully extracted
            from the Rigols
        """
        success = False
        # Get the time
        data = [time.time()-self.computerStartTime]
        # Measure the 4 channels
        reading = self.ps1.query(":MEAS:ALL? CH1")
        data += reading.split(",")
        reading = self.ps1.query(":MEAS:ALL? CH2")
        data += reading.split(",")
        reading = self.ps2.query(":MEAS:ALL? CH1")
        data += reading.split(",")
        reading = self.ps2.query(":MEAS:ALL? CH2")
        data += reading.split(",")
        # Check if the data is the correct shape
        if(len(data) == 13):
            success = True
        return(data, success)

    def recovery(self):
        """
        Recovery code to be run in the event that a connection to a power
        supply unit or the DMM is lost. This code checks for VISA connections
        every 2 connections until if finds 3. After this, it calls the
        initializeScan function and allows data taking to resuem.

        Returns
        -------
        None.

        """
        recovering = True
        if(self.saveIndex!=self.data.shape[1]):
            np.save("tempFiles/temp_"+str(int(time.time()))+".npy",
                    self.data[:, self.saveIndex:])
            self.saveIndex = self.data.shape[1]
        
        # Run a while loop until connections have recovered
        while(recovering):
            print("In Recovery Loop")
            # Check for number of VISA connections
            allAddresses = self.rm.list_resources()
            timeVal = str(int(time.time()))
            if(len(allAddresses) != 3):
                # If there aren't three connections, log it, then sleep
                # for two seconds
                self.log_file.write('Recovery|0|FAIL|' + timeVal+"\n")
                time.sleep(2)
            else:
                # If there are three connections, log it, then close the old
                # DMM connections and try again
                recovering = False
                self.log_file.write('Recovery|0|SUCCESS|' + timeVal+"\n")

                # If there are issues with some of the connections they
                # may already be closed which could throw an error.
                # This handles those possible errors.
                for connection in [self.dmm, self.ps1, self.ps2]:
                    try:
                        connection.close()
                    except:
                        pass

        # Having recovered the connection run initializeSan again
        self.initializeScan()

    def setPS(self, unit, channel, value):
        """
        Sets a power supply channel to the requested value if it is allowed,
        does nothing otherwise.

        If the values are allowed but the power supply fails to respond then
        recovery is launched and the original function call is called up again

        Parameters
        ----------
        unit : Int
            The power supply unit to adjust
        channel : Int
            The power supply channel to adjust
        value : Double
            The new max voltage to set for the channel

        Returns
        -------
        None.

        """

        # Verify that the combination of units, channels, and value is allowed
        if(unit == 1):
            if(channel == 1):
                if(value > 60 or value < 0):
                    return()
            if(channel == 2):
                if(value > 8 or value < 0):
                    return()
        if(unit == 2):
            if(channel == 1):
                if(value > 60 or value < 0):
                    return()
            if(channel == 2):
                if(value > 8 or value < 0):
                    return()

        # Blocks to handle errors
        try:
            # Set the new voltage and check that it is the same
            ps = [self.ps1, self.ps2]
            message = ":APPL CH" + str(channel) + "," + str(value) + ",1"
            ps[unit-1].write(message)
            voltage = ps[unit-1].query(":APPL? CH"+str(channel))
            voltage = voltage.split(",")
            if(len(voltage) == 3):  # Verify the form of the PS output
                voltage = voltage[1]
                if(float(voltage) == value):  # Voltage set properly
                    message = 'PS' + str(unit) + 'C' + str(channel) + '|'
                    message += str(value) + '|SUCCESS|'
                    message += str(int(time.time())) + "\n"
                    self.log_file.write(message)
                    key = 'PS' + str(unit) + 'C' + str(channel)
                    self.psValues[key] = value
                else:  # Voltage set incorrectly
                    message = 'PS' + str(unit) + 'C' + str(channel) + '|'
                    message += voltage[1] + '|PARTIALFAIL|'
                    message += str(int(time.time())) + "\n"
                    self.log_file.write(message)
                    key = 'PS' + str(unit) + 'C' + str(channel)
                    self.psValues[key] = float(voltage)
            else:  # Output of PS not the correct form
                message = 'PS' + str(unit) + 'C' + str(channel) + '|'
                message += str(value) + '|FAIL|' + str(int(time.time()))
                message += "\n"
                self.log_file.write(message)
        except:  # PS fails to respond
            message = 'PS' + str(unit) + 'C' + str(channel) + '|' + str(value)
            message += '|FAIL|' + str(int(time.time()))+"\n"
            self.log_file.write(message)
            self.recovery()
            self.updateData(unit, channel, value)

    def gridFunction(self):
        """Instantiate the Grid object to display recent data."""
        GridData(self)

    def biasFunction(self):
        """Creates a Form object to adjust bias voltage."""
        Form(self, ["Voltage"], ["Channel", "Range"], "bias")

    def graphFunction(self):
        """Creates a Form object to create a custom graph."""
        Form(self, ["Current", "SIPM Output Voltage", "Temperature",
                    "Radiation", "Power Supply Voltage", "Voltage Histogram"],
             ["Channel", "Past Hours"], "graph")

    def noteFunction(self):
        """Create a Note object to allow the user to create a note."""
        Note(self)

    def rotateGraphs(self):
        """
        Code which updates the four graphs displayed in the main UI. This code
        is called again five seconds later

        Returns
        -------
        None.

        """
        for x in range(4):
            # Check the four graph codes at the current location in the graph
            # cycle and plot the appropriate graphs
            graphCode = self.graphCycle[x][self.graphCycleLocation]
            if(graphCode[0] == "V"):
                self.voltageGraph(int(graphCode[1]), -1, x)
            elif(graphCode[0] == "T"):
                self.temperatureGraph(int(graphCode[1]), -1, x)
            elif(graphCode[0] == "C"):
                self.currentGraph(int(graphCode[1]), -1, x)
            elif(graphCode[0] == "P"):
                self.powerSupplyGraph(int(graphCode[1]), -1, x)
            elif(graphCode[0] == "R"):
                self.radiationFlagGraph(int(graphCode[1]), -1, x)
            elif(graphCode[0] == "H"):
                self.voltageHist(x)

        # Update the location in the graph cycle for next function call
        self.graphCycleLocation += 1
        self.graphCycleLocation = self.graphCycleLocation % 3
        self.after(5000, self.rotateGraphs)

    def voltageGraph(self, channelNumber, offset, graphNumber):
        """
        Creates a graph of a SIPM output voltage channel.

        Parameters
        ----------
        channelNumber : int
            The channel of the SIPM output voltage to display
        offset : int
            The past number of hours to show, defaults to all data if 0.
        graphNumber :  int
            The location to graph this. 0-3 correspond to the main GUI, 4
            corresponds to a popout window.

        Returns
        -------
        None

        """
        if(channelNumber not in [1, 2, 3]):  # Verify we have a valid channel
            return()
        if(len(self.data) > 0):  # Verify we have data
            # Get the data and the graph ranges
            voltageVals = self.data[4 + 2*(channelNumber-1), :]
            timeVals = self.data[4 + 2*(channelNumber-1)+1, :]
            maxTime = max(timeVals)*1.001
            if(offset < 0):
                minTime = 0
            else:
                minTime = max([maxTime-3600*offset, min(timeVals)])
            minY = min(voltageVals)*0.9
            maxY = max(voltageVals)*1.1
            if(graphNumber == 4):  # Make a popout graph
                title = "SIPM Output Voltage vs. Time for Channel "
                title += str(channelNumber)
                xlabel, ylabel = "Time (s)", "Voltage (V)"
                channel = 4 + 2*(channelNumber-1)
                data = [timeVals, voltageVals]
                xlim, ylim = [minTime, maxTime], [minY, maxY]
                graphType, source = "plot", self
                Graph(title, xlabel, ylabel, channel, data, xlim, ylim,
                      graphType, source)
            else:  # Make the graph on the main GUI
                self.ax[graphNumber].clear()
                self.ax[graphNumber].scatter(timeVals, voltageVals)
                self.ax[graphNumber].set_xlim(minTime, maxTime)
                self.ax[graphNumber].set_ylim(minY, maxY)
                self.ax[graphNumber].set_xlabel("Time (s)")
                self.ax[graphNumber].set_ylabel("Voltage (V)")
                title = "SIPM Output Voltage vs. Time for Channel "
                title += str(channelNumber)
                self.ax[graphNumber].set_title(title)
                self.ax[graphNumber]
                self.fig.tight_layout()
                self.canvas.draw()

    def powerSupplyGraph(self, channelNumber, offset, graphNumber):
        """
        Creates a graph of a power supply output voltage channel.

        Parameters
        ----------
        channelNumber : int
            The channel of the SIPM output voltage to display
        offset : int
            The past number of hours to show, defaults to all data if 0.
        graphNumber :  int
            The location to graph this. 0-3 correspond to the main GUI, 4
            corresponds to a popout window.

        Returns
        -------
        None

        """
        if(channelNumber not in [1, 2]):  # Verify we have a valid channel
            return()
        if(len(self.data) > 0):  # Verify we have data
            # Get the data and the graph ranges
            voltageVals = self.data[10 + 2*(channelNumber-1), :]
            timeVals = self.data[10 + 2*(channelNumber-1)+1, :]
            maxTime = max(timeVals)*1.001
            if(offset < 0):
                minTime = 0
            else:
                minTime = max([maxTime-3600*offset, min(timeVals)])
            minY = min(voltageVals)*0.9
            maxY = max(voltageVals)*1.1
            if(graphNumber == 4):  # Make a popout graph
                title = "Power Supply Input Voltage vs. Time for Channel "
                title = title + str(channelNumber)
                xlabel, ylabel = "Time (s)", "Voltage (V)"
                channel = 10 + 2*(channelNumber-1)
                data = [timeVals, voltageVals]
                xlim, ylim = [minTime, maxTime], [minY, maxY]
                graphType, source = "powerSupply", self
                Graph(title, xlabel, ylabel, channel, data, xlim, ylim,
                      graphType, source)
            else:  # Make the graph on the main GUI
                self.ax[graphNumber].clear()
                self.ax[graphNumber].scatter(timeVals, voltageVals)
                self.ax[graphNumber].set_xlim(minTime, maxTime)
                self.ax[graphNumber].set_ylim(minY, maxY)
                self.ax[graphNumber].set_xlabel("Time (s)")
                self.ax[graphNumber].set_ylabel("Voltage (V)")
                title = "Power Supply Input Voltage vs. Time for Channel "
                title = title + str(channelNumber)
                self.ax[graphNumber].set_title(title)
                self.canvas.draw()

    def temperatureGraph(self, channelNumber, offset, graphNumber):
        """
        Creates a graph of a temperature channel.

        Parameters
        ----------
        channelNumber : int
            The channel of the SIPM output voltage to display
        offset : int
            The past number of hours to show, defaults to all data if 0.
        graphNumber :  int
            The location to graph this. 0-3 correspond to the main GUI, 4
            corresponds to a popout window.

        Returns
        -------
        None

        """
        print(channelNumber, offset, graphNumber)
        if(channelNumber not in [1, 2, 3]):  # Verify we have a valid channel
            return()
        if(len(self.data) > 0):  # Verify we have data
            # Get the data and the graph ranges
            temperatureVals = self.data[14+2*(channelNumber-1), :]
            timeVals = self.data[14+2*(channelNumber-1)+1, :]
            maxTime = max(timeVals)*1.001
            if(offset < 0):
                minTime = 0
            else:
                minTime = max([maxTime-3600*offset, min(timeVals)])
            minY = min(temperatureVals)*0.9
            maxY = max(temperatureVals)*1.1
            if(graphNumber == 4):  # Make a popout graph
                title = "Temperature vs. Time for Channel " + str(channelNumber)
                xlabel, ylabel = "Time (s)", "Temperature (C)"
                channel = 14 + 2*(channelNumber-1)
                data, xlim = [timeVals, temperatureVals], [minTime, maxTime]
                ylim, graphType, source = [minY, maxY], "plot", self
                Graph(title, xlabel, ylabel, channel, data, xlim, ylim,
                      graphType, source)
            else:  # Make the graph on the main GUI
                self.ax[graphNumber].clear()
                self.ax[graphNumber].scatter(timeVals, temperatureVals)
                self.ax[graphNumber].set_xlim(minTime, maxTime)
                self.ax[graphNumber].set_ylim(minY, maxY)
                self.ax[graphNumber].set_xlabel("Time (s)")
                self.ax[graphNumber].set_ylabel("Temperature (C)")
                title = "Temperature vs. Time for Channel " + str(channelNumber)
                self.ax[graphNumber].set_title(title)
                self.canvas.draw()

    def currentGraph(self, channelNumber, offset, graphNumber):
        """
        Creates a graph of a current channel.

        Parameters
        ----------
        channelNumber : int
            The channel of the SIPM output voltage to display
        offset : int
            The past number of hours to show, defaults to all data if 0.
        graphNumber :  int
            The location to graph this. 0-3 correspond to the main GUI, 4
            corresponds to a popout window.

        Returns
        -------
        None

        """
        if(channelNumber not in [1]):  # Verify we have a valid channel
            return()
        if(len(self.data) > 0):  # Verify we have data
            # Get the data and the graph ranges
            currentVals = self.data[20+2*(channelNumber-1), :]
            timeVals = self.data[20+2*(channelNumber-1)+1, :]
            maxTime = max(timeVals)*1.001
            if(offset < 0):
                minTime = 0
            else:
                minTime = max([maxTime-3600*offset, min(timeVals)])
            minY = min(currentVals)*0.9
            maxY = max(currentVals)*1.1
            if(graphNumber == 4):  # Make a popout graph
                title = "Current vs. Time for Channel " + str(channelNumber)
                xlabel, ylabel = "Time (s)", "Current (A)"
                channel = 20+2*(channelNumber-1)
                data, xlim = [timeVals, currentVals], [minTime, maxTime]
                ylim, graphType, source = [minY, maxY], "plot", self
                Graph(title, xlabel, ylabel, channel, data, xlim, ylim,
                      graphType, source)
            else:  # Make the graph on the main GUI
                self.ax[graphNumber].clear()
                self.ax[graphNumber].scatter(timeVals, currentVals)
                self.ax[graphNumber].set_xlim(minTime, maxTime)
                self.ax[graphNumber].set_ylim(minY, maxY)
                self.ax[graphNumber].set_xlabel("Time (s)")
                self.ax[graphNumber].set_ylabel("Current (micro A)")
                title = "Current vs. Time for Channel " + str(channelNumber)
                self.ax[graphNumber].set_title(title)
                self.canvas.draw()

    def radiationFlagGraph(self, channelNumber, offset, graphNumber):
        """
        Creates a graph of a radiation flag channel.

        Parameters
        ----------
        channelNumber : int
            The channel of the SIPM output voltage to display
        offset : int
            The past number of hours to show, defaults to all data if 0.
        graphNumber :  int
            The location to graph this. 0-3 correspond to the main GUI, 4
            corresponds to a popout window.

        Returns
        -------
        None

        """
        if(channelNumber not in [1, 2]):  # Verify we have a valid channel
            return()

        if(len(self.data) > 0):  # Verify we have data
            # Get the data and the graph ranges
            voltageVals = self.data[2*(channelNumber-1), :]
            timeVals = self.data[2*(channelNumber-1)+1, :]
            maxTime = max(timeVals)*1.001
            if(offset < 0):
                minTime = 0
            else:
                minTime = max([maxTime-3600*offset, min(timeVals)])
            minY = min(voltageVals)*0.9
            maxY = max(voltageVals)*1.1
            if(graphNumber == 4):  # Make a popout graph
                title = "Radiation Flag " + str(channelNumber)
                title += " Voltage vs. Time"
                xlabel, ylabel = "Time (s)", "Voltage (V)"
                channel, data = 2*(channelNumber-1), [timeVals, voltageVals]
                xlim, ylim = [minTime, maxTime], [minY, maxY]
                graphType, source = "plot", self
                Graph(title, xlabel, ylabel, channel, data, xlim, ylim,
                      graphType, source)
            else:  # Make the graph on the main GUI
                self.ax[graphNumber].clear()
                self.ax[graphNumber].scatter(timeVals, voltageVals)
                self.ax[graphNumber].set_xlim(minTime, maxTime)
                self.ax[graphNumber].set_ylim(minY, maxY)
                self.ax[graphNumber].set_xlabel("Time (s)")
                self.ax[graphNumber].set_ylabel("Voltage (V)")
                title = "Radiation Flag " + str(channelNumber)
                title += " Voltage vs. Time"
                self.ax[graphNumber].set_title(title)
                self.canvas.draw()

    def voltageHist(self, graphNumber):
        """
        Creates a histogram of the most recent SIPM output voltages

        Parameters
        ----------
        channelNumber : int
            The channel of the SIPM output voltage to display
        offset : int
            The past number of hours to show, defaults to all data if 0.
        graphNumber :  int
            The location to graph this. 0-3 correspond to the main GUI, 4
            corresponds to a popout window.

        Returns
        -------
        None

        """
        if(len(self.data) > 0):  # Verify we have data
            # Get the data and the graph ranges
            voltages1 = []
            voltages2 = []
            for x in range(1):
                voltages1.append(self.data[4 + 2 * x, -10:])
            for x in range(2):
                voltages2.append(self.data[6 + 2 * x, -10:])
            voltages1 = np.concatenate(voltages1)
            mean1 = np.round(np.mean(voltages1), 3)
            sd1 = np.round(np.std(voltages1), 3)
            voltages2 = np.concatenate(voltages2)
            mean2 = np.round(np.mean(voltages2), 3)
            sd2 = np.round(np.std(voltages2), 3)

            if(graphNumber == 4):  # Make a popout graph
                title = "SIPM Voltage Distribution"
                ylabel, xlabel = "Counts", "Voltage (V)"
                data = self.getVoltageData()
                channel, xlim, ylim = 2, [0, 0], [0, 0],
                graphType, source = "hist", self
                Graph(title, xlabel, ylabel, channel, data, xlim, ylim,
                      graphType, source)
            else:  # Make the graph on the main GUI
                self.ax[graphNumber].clear()
                self.ax[graphNumber].hist(voltages1, bins=10,
                                          label="Channels 1-2",
                                          histtype="step", color="r")
                self.ax[graphNumber].axvline(mean1-sd1,
                                             label="Channels 1-2 Mean +/- sd",
                                             color="r", linestyle="--")
                self.ax[graphNumber].axvline(mean1+sd1, color="r",
                                             linestyle="--")
                self.ax[graphNumber].hist(voltages2, bins=10,
                                          label="Channels 3-3",
                                          histtype="step", color="k")
                self.ax[graphNumber].axvline(mean2-sd2, color="k",
                                             label="Channels 3-3 Mean +/- sd",
                                             linestyle="--")
                self.ax[graphNumber].axvline(mean2+sd2, color="k",
                                             linestyle="--")
                self.ax[graphNumber].set_ylim(0, 25)

                self.ax[graphNumber].legend()
                self.ax[graphNumber].set_xlabel("Voltage (V)")
                self.ax[graphNumber].set_ylabel("Counts")
                self.ax[graphNumber].set_title("SIPM Voltage Distribution")
                self.canvas.draw()

    def getRecentData(self):
        """
        Utility function that returns the most recent data collected from the
        DMM.
        """
        return(self.data[:, -1])

    def getChannelData(self, channel):
        """
        Utility function that returns all the data for a given channel
        """
        return(self.data[channel, :])

    def getVoltageData(self):
        """
        Utility function for the histogram graphs. This returns a list with
        the SIPM output voltages, mean, and sd from one set of the channels,
        and the same values for the other set of channels as well.
        """
        voltages1 = []
        voltages2 = []
        for x in range(1):
            voltages1.append(self.data[4+2*x, -10:])
        for x in range(2):
            voltages2.append(self.data[6+2*x, -10:])
        voltages1 = np.concatenate(voltages1)
        mean1 = np.round(np.mean(voltages1), 3)
        sd1 = np.round(np.std(voltages1), 3)
        voltages2 = np.concatenate(voltages2)
        mean2 = np.round(np.mean(voltages2), 3)
        sd2 = np.round(np.std(voltages2), 3)
        return([voltages1, mean1, sd1, voltages2, mean2, sd2])

    def giveGraphResults(self, measurement, value):
        """
        A function used by the graph form to return its results to this main
        object. It interprets the results and then creates the appropriate pop
        out graph.

        Parameters
        ----------
        measurement : String
            The graph to create
        value : String list
            A list with the channel for the graph and the past hours to graph.

        Returns
        -------
        None.
        """

        # Determine th channel
        channel = value[0]
        if(measurement == "Voltage Histogram"):
            channel = "0"
        pastHours = value[1]
        # Assuming a channel was given, create the appropriate graph
        if(len(channel) > 0):
            channel = int(channel)
            if(len(pastHours) > 0):
                pastHours = float(pastHours)
            else:
                pastHours = -1
            if(measurement == "Current"):
                self.currentGraph(channel, pastHours, 4)
            elif(measurement == "SIPM Output Voltage"):
                self.voltageGraph(channel, pastHours, 4)
            elif(measurement == "Temperature"):
                self.temperatureGraph(channel, pastHours, 4)
            elif(measurement == "Radiation"):
                self.radiationFlagGraph(channel, pastHours, 4)
            elif(measurement == "Power Supply Voltage"):
                self.powerSupplyGraph(channel, pastHours, 4)
            elif(measurement == "Voltage Histogram"):
                self.voltageHist(4)

    def giveBiasResults(self, value):
        """
        A function used by the bias form to return its results to this main
        object. It interprets the results and then adjuts the bias voltages
        from the power supplies appropriately.

        Parameters
        ----------
        value : String list
            A list with the channel for the bias adjustemtn and the new bias
            value

        Returns
        -------
        None.
        """

        # Check for a channel
        channel = value[0]
        if(len(channel) > 0):
            channel = int(channel)
        else:
            channel = channel - 1

        # Check for a new bias voltage
        val = value[1]
        if(len(val) > 0):
            val = float(val)
        else:
            channel = channel - 1

        # Figure out the unit number
        if(channel > 2):
            unit = 2
            channel = channel-2
        else:
            unit = 1
        self.setPS(unit, channel, val)

    def giveNote(self, note):
        """
        Saves a note given by the user to the log file with a time stamp

        Parameters
        ----------
        note : String
            Note given by user

        Returns
        -------
        None.

        """
        timeVal = time.time()
        message = 'UserNote|' + note + '|SUCCESS|' + str(timeVal)+"\n"
        self.log_file.write(message)

    def on_closing(self):
        """
        Action to take when a user attempts to close the program. This verifies
        their intention after informing them that this will stop data
        acquisiton. If the code is executed it saves all the data to a file
        and closes all the VISA connections aftert stopping the DMM scan.

        Returns
        -------
        None.

        """

        if tk.messagebox.askokcancel("Quit", "Are you sure you want to quit?" +
                                     " This will stop data acquisition."):

            # Save a full copy of the data
            np.save("full_"+str(int(time.time()))+".npy", self.data)
            np.save("fullRigol_"+str(int(time.time()))+".npy", self.rigolData)
            self.log_file.close()

            # Stop the scans and close VISA connections
            self.dmm.write(":ABORt")
            self.dmm.close()
            self.ps1.close()
            self.ps2.close()

            # Destroy the GUI
            self.quit()
            self.destroy()


class Form(tk.Toplevel):
    """
    A utility object used to obtain information from the user with a dropdown
    box.
    """
    def __init__(self, root, options, parameters, formType):
        """
        Create the Form

        Parameters
        ----------
        root : MainUI
            MainUI object that created the Form object
        options: String list
            options for the dropdown box
        paramters: String list
            Prompts for user fill in data
        formType: String
            Either graph or bias, denoting which form this is

        Returns
        -------
        None.

        """
        super().__init__()
        self.formType = formType
        self.root = root
        # Dropdown menu options
        self.options = options
        # datatype of menu text
        self.clicked = tk.StringVar()

        # initial menu text
        self.clicked.set(options[0])

        self.labels = []
        self.values = []

        # Create Dropdown menu
        self.measurementLabel = tk.Label(self, text="Measurement")
        self.measurementLabel.grid(row=0, column=0)
        self.measurementValue = tk.OptionMenu(self, self.clicked,
                                              *self.options)
        self.measurementValue.grid(row=0, column=1)
        # Create the short user entered boxes.
        for x in range(len(parameters)):
            self.labels.append(tk.Label(self, text=parameters[x]))
            self.labels[-1].grid(row=1+x, column=0, columnspan=1,
                                 sticky="NESW")

            self.values.append(tk.Entry(self))
            self.values[-1].grid(row=1+x, column=1, columnspan=1,
                                 sticky="NESW")

        # Create button, it will change label text
        self.button = tk.Button(self, text="Submit", command=self.submit)
        self.button.grid(row=1+len(parameters), columnspan=2, sticky="NESW")

    # Change the label text
    def submit(self):
        """
        Gets the results from the form, returns them to the main UI object,
        then deletes this object.
        """
        # Get the drop down
        measurement = self.clicked.get()

        # Get the user inputs and sanitize them
        values = []
        for x in range(len(self.values)):
            values.append(self.sanitize(self.values[x].get()))

        # Return the results to the main UI object
        if(self.formType == "graph"):
            self.root.giveGraphResults(measurement, values)
        elif(self.formType == "bias"):
            self.root.giveBiasResults(values)
        self.destroy()

    def sanitize(self, val):
        """
        Parameters
        ----------
        val : string
            An input string which is supposed to contain a number

        Returns
        -------
        number : string
            The input string with all none numerical characters removed

        """

        # Only keep numbers and periods in the input string
        temp = [x for x in val]
        number = ""
        for val in temp:
            if(val.isdigit() or val == "."):
                number += val
        return(number)


class Note(tk.Toplevel):
    def __init__(self, root):
        """
        A simple note object prompts the user for a note, returns it to the GUI
        that created the object, and then destroys itself.

        Parameters
        ----------
        root : tkinter object
            A pointer to the main GUI object

        Returns
        -------
        None.

        """
        super().__init__()

        self.root = root

        # Creates a prompt, text field, and submission button
        self.label = tk.Label(self, text="Type a note below:")
        self.label.grid(row=0, column=0)
        self.textInput = tk.Text(self, height=20, width=35)
        self.textInput.grid(row=1, column=0)
        self.button = tk.Button(self, text="Submit", command=self.submit)
        self.button.grid(row=2, column=0, sticky="NESW")

    def submit(self):
        """
        Get the data the user entered into the note field and return it
        to the main GUI through a giveNote function in the main GUI.

        Returns
        -------
        None.

        """

        # Get the note and return it
        note = self.textInput.get("1.0", "end-1c")
        self.root.giveNote(note)

        # Destroy this window
        self.destroy()


class Trigger(object):
    """
    A stand in object that currently does nothing. This may later have
    functionality added to automatically make adjustements to the experiment
    based on data
    """
    def __init__(self, source):
        self.source = source

    def check(self, data):
        pass


class GridData(tk.Toplevel):
    """
    An object used to show the numerical values of the most recent
    measurements.
    """
    def __init__(self, source):
        """
        Parameters
        ----------
        source : MainUI
            MainUI object that created the GridData object

        Returns
        -------
        None.

        """
        super().__init__()

        # Configure the GUI
        self.dataSource = source

        self.root = self

        self.dataView = ttk.Treeview(self.root, height=7)

        self.style = ttk.Style()
        self.style.configure("Treeview.Heading", font=(None, 18))
        self.style.configure("Treeview", font=(None, 18), rowheight=60)

        self.dataView['columns'] = ('measurement1', 'value1', 'blank',
                                    'measurement2', 'value2')
        self.measurementLabels = []

        # Add row labels
        for x in range(2):
            self.measurementLabels.append("Radiation Flags "+str(x+1)+" (V)")
        for x in range(3):
            self.measurementLabels.append("SIPM Voltage Out "+str(x+1)+" (V)")
        for x in range(2):
            self.measurementLabels.append("SIPM Bias Voltage "+str(x+1)+" (V)")
        for x in range(3):
            self.measurementLabels.append("Temperature "+str(x+1)+" (C)")
        for x in range(1):
            self.measurementLabels.append("Current Out "+str(x+1)+" (A)")

        # Add column labels
        self.dataView.column("#0", width=0, stretch="NO")
        self.dataView.column("measurement1", anchor="center", width=660)
        self.dataView.column("value1", anchor="center", width=200)
        self.dataView.column("blank", anchor="center", width=200)
        self.dataView.column("measurement2", anchor="center", width=660)
        self.dataView.column("value2", anchor="center", width=160)

        self.dataView.heading("#0", text="", anchor="center")
        self.dataView.heading("measurement1", text="Measurement",
                              anchor="center")
        self.dataView.heading("value1", text="Value", anchor="center")
        self.dataView.heading("blank", text="", anchor="center")
        self.dataView.heading("measurement2", text="Measurement",
                              anchor="center")
        self.dataView.heading("value2", text="Value", anchor="center")

        # Put in temporary data
        for x in range(5):
            self.dataView.insert(parent='', index='end', iid=x, text='',
                                 values=(self.measurementLabels[x],
                                         np.round(np.random.uniform(0, 1), 3),
                                         "", self.measurementLabels[x+6],
                                         np.round(np.random.uniform(0, 1), 3)))
        self.dataView.insert(parent='', index='end', iid=5, text='',
                             values=(self.measurementLabels[5],
                                     np.round(np.random.uniform(0, 1), 3), "",
                                     "", ""))

        # Format the GUI and schedule a data update
        self.dataView.pack()
        self.dataView.after(2000, self.updateData)

    def updateData(self):
        """
        Gets fresh data from the the main GUI object and updates the displayed
        data.
        """
        measurements = self.dataSource.getRecentData()
        measurements = np.round(measurements, 3)

        for x in range(5):
            self.dataView.item(x, values=(self.measurementLabels[x],
                                          measurements[x*2], "",
                                          self.measurementLabels[x+6],
                                          measurements[x*2+12]))
        self.dataView.item(5, values=(self.measurementLabels[5],
                                      measurements[10], "", "", ""))
        self.update()

        self.dataView.after(2000, self.updateData)


class Graph(tk.Toplevel):
    """
    This object handles a single popout graph. It periodically querries the
    main UI object for more recent data and updates the graph.
    """
    def __init__(self, title, xlabel, ylabel, channel, data, xlim, ylim,
                 graphType, source):
        """
        Parameters
        ----------
        title : String
            The title for the graph
        xlabel : String
            The label for the x axis of the graph
        ylabel : String
            The label for the y axis of the graph
        channel : Int
            The channel this graph corresponds to
        data : Float list
            Data for graphing
        xlim : Float list
            List with the min and max x values
        ylim : Float list
            List with the min and max y values
        graphType : String
            Details what kind of graph this is
        source : MainUI
            MainUI object that created the GridData object

        Returns
        -------
        None.
        """

        # Graph settings
        super().__init__()
        font = {'size': 18}
        matplotlib.rc('font', **font)
        matplotlib.rc('xtick', labelsize=18)
        matplotlib.rc('ytick', labelsize=18)

        # GUI settings
        self.root = self
        self.root.wm_title(title)
        self.channel = channel
        self.source = source
        self.fig = Figure(figsize=(8, 6), dpi=100)
        self.figPlot = self.fig.add_subplot(111)
        self.graphType = graphType

        # Graph info
        self.graphTitle = title
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.ylim = ylim
        self.xlim = xlim

        # Graph and Hist are different, make the acutal graphs
        if(self.graphType == "hist"):
            self.figPlot.hist(data[0], bins=10, label="Channels 1-10",
                              histtype="step", color="r")
            self.figPlot.axvline(data[1]-data[2],
                                 label="Channels 1-10 Mean +/- sd",
                                 color="r", linestyle="--")
            self.figPlot.axvline(data[1]+data[2], color="r", linestyle="--")
            self.figPlot.hist(data[3], bins=10, label="Channels 11-20",
                              histtype="step", color="k")
            self.figPlot.axvline(data[4]-data[5], color="k",
                                 label="Channels 11-20 Mean +/- sd",
                                 linestyle="--")
            self.figPlot.axvline(data[4]+data[5], color="k", linestyle="--")
            self.figPlot.set_title(title)
            self.figPlot.set_xlabel(xlabel)
            self.figPlot.set_ylabel(ylabel)
        else:
            self.figPlot.scatter(data[0], data[1])
            if(len(self.xlim) == 2):
                self.figPlot.set_xlim(xlim[0], xlim[1])
            if(len(self.ylim) == 2):
                self.figPlot.set_ylim(ylim[0], ylim[1])
            self.figPlot.set_title(title)
            self.figPlot.set_xlabel(xlabel)
            self.figPlot.set_ylabel(ylabel)

        # Put the graph on the canvas and GUI
        self.canvas = FigureCanvasTkAgg(self.fig, master=self.root)
        self.fig.tight_layout()
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side="top", fill="both", expand=1)

        toolbar = NavigationToolbar2Tk(self.canvas, self.root)
        toolbar.update()
        self.canvas.get_tk_widget().pack(side="top", fill="both", expand=1)

        self.canvas.mpl_connect("key_press_event", self.onKeyPress)

        self.root.after(2000, self.updateData)

    def updateData(self):
        """
        Obtains new data for the graph from the source object and updates the
        graph
        """

        # Clear old graph
        self.figPlot.clear()

        # Make histogram if histogram
        if(self.graphType == "hist"):
            data = self.source.getVoltageData()
            self.figPlot.hist(data[0], bins=10, label="Channels 1-10",
                              histtype="step", color="r")
            self.figPlot.axvline(data[1]-data[2],
                                 label="Channels 1-10 Mean +/- sd",
                                 color="r", linestyle="--")
            self.figPlot.axvline(data[1]+data[2], color="r", linestyle="--")
            self.figPlot.hist(data[3], bins=10, label="Channels 11-20",
                              histtype="step", color="k")
            self.figPlot.axvline(data[4]-data[5], color="k",
                                 label="Channels 11-20 Mean +/- sd",
                                 linestyle="--")
            self.figPlot.axvline(data[4]+data[5], color="k", linestyle="--")
            self.figPlot.set_title(self.graphTitle)
            self.figPlot.set_xlabel(self.xlabel)
            self.figPlot.set_ylabel(self.ylabel)
        else:  # Make other kind of graph
            data = []
            print("channel", self.channel)
            data = [self.source.getChannelData(self.channel+1),
                    self.source.getChannelData(self.channel)]
            self.figPlot.scatter(data[0], data[1])
            # Adjust plot limits, keep previous xmin
            if(len(self.xlim) == 2):
                self.figPlot.set_xlim(self.xlim[0], max(data[0]*1.1))
            if(len(self.ylim) == 2):
                self.figPlot.set_ylim(min(data[1]*0.9), max(data[1]*1.1))
            self.figPlot.set_title(self.graphTitle)
            self.figPlot.set_xlabel(self.xlabel)
            self.figPlot.set_ylabel(self.ylabel)
        self.fig.tight_layout()
        self.canvas.draw()
        self.after(2000, self.updateData)

    def onKeyPress(self, event):
        """
        Handles a key press on the Graph GUI

        Parameters:
            event : the event from the GUI
        """
        print("you pressed {}".format(event.key))
        key_press_handler(event, self.canvas, self.toolbar)

    def on_closing(self):
        """
        Instructions for closing the GUI.
        """
        self.destroy()
        self.quit()

if(__name__ == "__main__"):
    ui = MainUI()
    ui.mainloop()
