# -*- coding: utf-8 -*-
"""
Created on Fri May 20 15:27:40 2022

@author: brade
"""

import numpy as np
import matplotlib.pyplot as plt

tSteps = 1000 # Total number of time steps
xSteps = 20 # Total number of x steps
ySteps = 200 # Total number of y steps
alpha = .1  # Step size, should not be greater than 0.5

dataGrid = np.ones([tSteps,xSteps, ySteps])  # Something in the material
dataGrid[:,0,:] = dataGrid[:,0,:]*0  # Nothing on one side
dataGrid[:,-1,:] = dataGrid[:,0,:]  # Nothing on other side

def step(data):
    newData = np.copy(data)
    newData[1:,1:-1, 1:-1] =  data[0:-1,1:-1,1:-1]
    newData[1:,1:-1, 1:-1] += alpha*(data[0:-1, 2:,1:-1] + data[0:-1, 0:-2,1:-1] -2*data[0:-1,1:-1,1:-1]) 
    newData[1:,1:-1, 1:-1] +=  alpha*(data[0:-1,1:-1, 2:] + data[0:-1,1:-1, 0:-2] -2*data[0:-1,1:-1,1:-1]) 
    
    newData[:,0,:25] = newData[:,1,:25] # Exit at front partially blocked
    newData[:,0,175:] = newData[:,1,175:]  # Exit at front partially blocked
    
    newData[:,1:-1,0] = newData[:,1:-1,1]  # No exit along sides
    newData[:,1:-1,-1] = newData[:,1:-1,-2]  # No exit along sides
    
    return(newData)
maxChange = 10
y=0
while(maxChange>0): # Max change will become zero eventuallly
    newData = step(dataGrid)
    maxChange = np.max(np.abs(newData-dataGrid))
    dataGrid = newData
    if((y+1)%100==0):
        print("Step", y+1)
        print("Max Change", maxChange)

        plt.figure()
        for x in range(10):  # Summarize current results
            plt.plot(dataGrid[tSteps*x//10,1,:])
        plt.plot(dataGrid[-1,1,:])
        plt.show()
    y+=1
saveFile = "diffusion_2d_front_tape_hard_wall_a_"+str(alpha)+"_t_"+str(tSteps)+"_x_"+str(xSteps)+".npy"
print(saveFile)
np.save(saveFile, dataGrid) # Save current grid
        
        
