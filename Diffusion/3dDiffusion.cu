#include <string>
#include <math.h> 
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <nvToolsExt.h>
#include <stdio.h>
#include <ctime>


#define alpha 0.05 // Numerical fit paramter, making it too big will cause the PDE to not converge
#define propConstant 5 // Related to solubility
#define innerFrac 0.3 // Conectration of O2 in center relative to tile
#define dimX 74 // Number of x bins, not recomened to play with this
#define dimY 340 // Number of y bins
#define dimZ 340 // Number of z bins
#define dimT 50000 // Number of t bins, may need to increase
#define DSIZE (74*340*340) //8554400
#define blocks 80 // Should be number of streaming multiprocessors x2 
#define threads 1024 // Should probably be 1024

#define cudaCheckErrors(msg)                                    \
    do {                                                        \
        cudaError_t __err = cudaGetLastError();                 \
        if (__err != cudaSuccess) {                             \
            fprintf(stderr, "Fatal error: %s (%s at %s:%d)\n",  \
                    msg, cudaGetErrorString(__err),             \
                    __FILE__, __LINE__);                        \
            fprintf(stderr, "*** FAILED - ABORTING\n");         \
            exit(1);                                            \
        }                                                       \
    } while (0)

// This code finishes calculating the average of the air gap
__global__ void reduce(float *gdata, float *out, size_t n, float sizeCenter) {
    __shared__ float sdata[blocks];
    
    int tid = threadIdx.x;
    sdata[tid] = 0.0f;
    size_t idx = threadIdx.x + blockDim.x * blockIdx.x;

    while (idx < n) {  // grid stride loop to load data
        sdata[tid] += gdata[idx];
        idx += gridDim.x*blockDim.x;  
    }

    // s should be the first power of 2 smaller than the number of blocks
    for (unsigned int s=64; s>0; s>>=1) {
        __syncthreads();
        if (tid < s  && tid + s < blocks) {
            // parallel sweep reduction
            sdata[tid] += sdata[tid + s];
        }
    }
    
    if (tid == 0){
        out[blockIdx.x] = sdata[0]/sizeCenter;
    }
}


__global__ void finiteDiff(const float *inputVal, float *outputVal, float *centerVal, float *reductionOut, int ds, int tStamp){
    
    __shared__ float sdata[threads];
    
    int midX = 37; // dimX/2;
    int midY = 170; // dimY/2;
    int midZ = 170; // dimZ/2;
    
    // Used to help find the mean of air gap
    int tid = threadIdx.x;
    sdata[tid] = 0.0f;

    for (int index = threadIdx.x+blockDim.x*blockIdx.x; index < ds; index+=gridDim.x*blockDim.x){   
        
        // Not boundary of air gap
        float lScale = 1;
        float rScale = 1;
        
        //Determine array location
        int x = index%dimX;
        int y = ((index-x)/dimX)%dimY;
        int z = (index - x - y*dimX)/dimY/dimX;
        
        // Check if boundary of air gpa
        if(x==42){
            lScale = propConstant;
        }
        if(x==31){
            rScale = propConstant;
        }
        // Assuming not a boundary of the array
        if(x>0 && y>0 && z>0 && x<dimX-1 && y<dimY-1 && z <dimZ-1){
            // The actual finite differences update
            outputVal[index] = inputVal[index];
            outputVal[index] += alpha*(inputVal[index-1]*lScale+inputVal[index+1]*rScale-(rScale+lScale)*inputVal[index]);
            outputVal[index] += alpha*(inputVal[index-dimX]+inputVal[index+dimX]-2*inputVal[index]);
            outputVal[index] += alpha*(inputVal[index-dimX*dimY]+inputVal[index+dimX*dimY]-2*inputVal[index]);
            
            //Record center value
            if(x==midX && y==midY && z==midZ){
                centerVal[tStamp] = outputVal[index];
            }
            
            //Record if center value
            if(x>31 && x<42){
                sdata[tid] += outputVal[index];
            }
            // Blocked by tape in the front and back 
            if((y<int(dimY/10)-0.5 || y>int(dimY*9/10)-0.5) || (z<int(dimZ/10)-0.5 || z>int(dimZ*9/10)-0.5)){
                if(x==1 || x == dimX-2){
                    int shift =1;
                    if(x==1){
                        shift = -1;
                    }
                    outputVal[index+shift] = outputVal[index];
                    if(y<1.5){
                        outputVal[index+shift-dimX] = outputVal[index];
                        if (z < 1.5){
                            outputVal[index-dimY*dimX-dimX+shift] = outputVal[index];
                        }
                    }
                    if(y>dimY-2.5){
                        outputVal[index+shift+dimX] = outputVal[index];
                        if (z > dimZ - 2.5){
                            outputVal[index+dimY*dimX+dimX+shift] = outputVal[index];
                        }
                    }
                    if(z<1.5){
                        outputVal[index+shift-dimX*dimY] = outputVal[index];
                        if (y > dimY - 2.5){
                            outputVal[index-dimY*dimX+dimX+shift] = outputVal[index];
                        }
                    }
                    if(z>dimZ-2.5){
                        outputVal[index+shift+dimX*dimY] = outputVal[index];
                        if (y < 1.5){
                            outputVal[index+dimY*dimX-dimX+shift] = outputVal[index];
                        }
                    }

                }
            }

            // No exits along the sides
            if(y < 1.5){
                outputVal[index-dimX] = outputVal[index];
                if (z < 1.5){
                    outputVal[index-dimY*dimX-dimX] = outputVal[index];
                }
            }
            if(y > dimY -2.5) {
                outputVal[index+dimX] = outputVal[index];
                if (z > dimZ - 2.5){
                    outputVal[index+dimY*dimX+dimX] = outputVal[index];
                }
            }
            if (z < 1.5){
                outputVal[index-dimY*dimX] = outputVal[index];
                if (y > dimY - 2.5){
                    outputVal[index-dimY*dimX+dimX] = outputVal[index];
                }
            }
            if (z > dimZ -2.5){
                outputVal[index+dimY*dimX] = outputVal[index];
                if (y < 1.5){
                    outputVal[index+dimY*dimX-dimX] = outputVal[index];
                }
            }
        }
        
    }
    
    // Do some work calculating the average fo the center
    for (unsigned int s=(blockDim.x)/2; s>0; s>>=1) {
        __syncthreads();
        if (tid < s && tid + s < threads) {
            // parallel sweep reduction
            sdata[tid] += sdata[tid + s];
        }
    }
    __syncthreads();
    if (tid == 0){
        reductionOut[blockIdx.x] = sdata[0];
    }
}


__global__ void setCenter(float *outputVal, int ds, float *centerVal){

    // Update the center of the grid with the average 
    for (int idx = threadIdx.x+blockDim.x*blockIdx.x; idx < ds; idx+=gridDim.x*blockDim.x){   
        
        int smallXDim = 10;


        int x = idx%smallXDim;
        int y = ((idx-x)/smallXDim)%dimY;
        int z = (idx - x - y*smallXDim)/(dimY*smallXDim);
        x += 32;
        
        int index = x + y*dimX + z*(dimX*dimY);

        outputVal[index] = centerVal[0];

    }

}


//Take one step of the algorithm
void stepAlgo(float *d_inputVal, float *d_outputVal, float *d_centerVal, float *d_reductionData, float *d_mean, int tStamp){

    // Run the main algorithm
    finiteDiff<<<blocks, threads>>>(d_inputVal, d_outputVal, d_centerVal, d_reductionData, DSIZE, tStamp);
    cudaCheckErrors("main kernel launch failure");
    
    // Get the average
    reduce<<<1, blocks>>>(d_reductionData, d_mean, blocks, float(10*(dimY-2)*(dimZ-2))); // reduce stage 2
    cudaCheckErrors("reduction kernel launch failure");
    
    // Set the average of the center
    setCenter<<<blocks, threads>>>(d_outputVal, 10*dimY*dimZ, d_mean);
    cudaCheckErrors("center kernel launch failure");
    cudaDeviceSynchronize();

}


void run(float *inputVal, float *centerData){
    int counter = 1;
    
    // Declare device pointers
    float *d_inputVal;
    float *d_outputVal;
    float *d_centerData;
    float *d_reductionData;
    float *d_mean;

    // Allocate memory on the gpu
    cudaMalloc(&d_inputVal, DSIZE*sizeof(float));
    cudaMalloc(&d_outputVal, DSIZE*sizeof(float));
    cudaMalloc(&d_centerData, dimT*sizeof(float));
    cudaMalloc(&d_reductionData, blocks*sizeof(float));
    cudaMalloc(&d_mean, sizeof(float));  
    cudaCheckErrors("cudaMalloc failure"); // error checking

    // Copy data to the GPU
    cudaMemcpy(d_inputVal, inputVal, DSIZE*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_outputVal, inputVal, DSIZE*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_centerData, centerData, dimT*sizeof(float), cudaMemcpyHostToDevice);
    cudaCheckErrors("cudaMemcpy H2D failure");
    
    
    std::time_t msStart = std::time(nullptr);
    while(counter<=dimT) {
        // Run 1 step of the algorithm
        stepAlgo(d_inputVal, d_outputVal, d_centerData, d_reductionData, d_mean, counter-1);
        if(counter%100==0){
            std::cout << counter << std::endl;
        }
        counter++;
        
        // Run another step but with the input and output arrays flipped so
        // the memory doesn't need copied
        stepAlgo(d_outputVal, d_inputVal, d_centerData, d_reductionData, d_mean, counter-1);
        if(counter%100==0){
            std::cout << counter << std::endl;
        }
        counter++;
    }
    std::time_t msEnd = std::time(nullptr);
    
    // Give timing information
    std::cout << double(msEnd-msStart)*double(1000)/double(counter) << " ms per step\n";
   
    // Copy data off the GPU
    cudaMemcpy(centerData, d_centerData, dimT*sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(inputVal, d_inputVal, DSIZE*sizeof(float), cudaMemcpyDeviceToHost);
    
    cudaCheckErrors("kernel execution failure or cudaMemcpy H2D failure");
    
    // Free the memory on the GPU
    cudaFree(d_inputVal);
    cudaFree(d_outputVal);
    cudaFree(d_centerData);
    cudaFree(d_reductionData);
    cudaFree(d_mean);

    

}

int main(int argc, char *argv[]){
    
    // Allocate arrays for data storage
    float *inputArray = new float[DSIZE];
    float *centerData = new float[dimT];

    
    for(int z=0; z<dimZ; z++){
        for(int y=0; y<dimY; y++){
            // Concentration of 1 in the tile
            for(int x=0; x<31; x++){
                int index = x + dimX*(y+z*dimY);
                inputArray[index] = 1;
            }

            // Concentration of innerFrac in the gap
            for(int x=31; x<42; x++){
                int index = x + dimX*(y+z*dimY);
                inputArray[index] = innerFrac;
            }

            // Concentration of 1 in the tile
            for(int x=42; x<dimX; x++){
                int index = x + dimX*(y+z*dimY);
                inputArray[index] = 1;
            }
        }

    }
    
    // Concentratino of 0 outside the tile
    for(int y=0; y<dimY; y++){
        for(int z=0; z<dimZ; z++){
            int index = 0 + dimX*(y+z*dimY);
            inputArray[index] = 0;
            index = (dimX-1) + dimX*(y+z*dimY);
            inputArray[index] = 0;
        }
    }

    // Boundary conditions from the tape
    for(int y=1; y<dimY-1; y++){
        for(int z=1; z<dimZ-1; z++){
            if((y<int(dimY/10)-0.5 || y>int(dimY*9/10)-0.5) || (z<int(dimZ/10)-0.5 || z>int(dimZ*9/10)-0.5)){
                int x=1;
                int index = x + dimX*(y+dimY*z);
                inputArray[index-1] = inputArray[index];
                if(y<1.5){
                    inputArray[index-1-dimX] = inputArray[index];
                    if (z < 1.5){
                        inputArray[index-dimY*dimX-dimX-1] = inputArray[index];
                    }
                }
                if(y>dimY-2.5){
                    inputArray[index-1+dimX] = inputArray[index];
                    if (z > dimZ - 2.5){
                        inputArray[index+dimY*dimX+dimX-1] = inputArray[index];
                    }
                }
                if(z<1.5){
                    inputArray[index-1-dimX*dimY] = inputArray[index];
                    if (y > dimY - 2.5){
                        inputArray[index-dimY*dimX+dimX-1] = inputArray[index];
                    }
                }
                if(z>dimZ-2.5){
                    inputArray[index-1+dimX*dimY] = inputArray[index];
                    if (y < 1.5){
                        inputArray[index+dimY*dimX-dimX-1] = inputArray[index];
                    }
                }

                x=dimX-2;
                index = x + dimX*(y+dimY*z);
                inputArray[index+1] = inputArray[index];
                if(y<1.5){
                    inputArray[index+1-dimX] = inputArray[index];
                    if (z < 1.5){
                        inputArray[index-dimY*dimX-dimX+1] = inputArray[index];
                    }
                }
                if(y>dimY-2.5){
                    inputArray[index+1+dimX] = inputArray[index];
                    if (z > dimZ - 2.5){
                        inputArray[index+dimY*dimX+dimX+1] = inputArray[index];
                    }
                }
                if(z<1.5){
                    inputArray[index+1-dimX*dimY] = inputArray[index];
                    if (y > dimY - 2.5){
                        inputArray[index-dimY*dimX+dimX+1] = inputArray[index];
                    }
                }
                if(z>dimZ-2.5){
                    inputArray[index+1+dimX*dimY] = inputArray[index];
                    if (y < 1.5){
                        inputArray[index+dimY*dimX-dimX+1] = inputArray[index];
                    }
                }
            }
        }
    }


    // Store the initial array in a binary file
    FILE *dataStart = fopen("dataStart.dat", "wb");
    fwrite(inputArray, sizeof(float), DSIZE, dataStart);
    fclose(dataStart);

    // Run the algorithm
    run(inputArray, centerData);

    // Store the center data in a binary file
    FILE *data = fopen("data.dat", "wb");
    fwrite(centerData, sizeof(float), dimT, data);
    fclose(data);

    // Store the final array in a binary file
    FILE *dataFull = fopen("dataFull.dat", "wb");
    fwrite(inputArray, sizeof(float), DSIZE, dataFull);
    fclose(dataFull);
    
    // Free memory
    free(inputArray);
    free(centerData);
    
    return(0);
}    
