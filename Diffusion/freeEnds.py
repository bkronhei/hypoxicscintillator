# -*- coding: utf-8 -*-
"""
Created on Fri May 20 15:27:40 2022

@author: brade
"""

import numpy as np
import matplotlib.pyplot as plt

tSteps = 3000
xSteps = 99
dataGrid = np.ones([tSteps,xSteps])
dataGrid[:,0] = dataGrid[:,0]*0
dataGrid[:,-1] = dataGrid[:,0]

#plt.imshow(dataGrid)
alpha = .5

def step(inputData):
    data = np.copy(inputData)
    data[1:,1:-1] = alpha*(data[0:-1, 2:] + data[0:-1, 0:-2]) + (1-2*alpha)*data[0:-1,1:-1]
    return(data)
maxChange = 10
y=0
while(maxChange>0): # Max change will become zero eventuallly
    newData = step(dataGrid)
    maxChange = np.max(np.abs(newData-dataGrid))
    dataGrid = newData
    if((y+1)%300==0):
        print("Step", y+1)
        print("Max Change", maxChange)
        plt.figure()
        for x in range(10):
            plt.plot(dataGrid[tSteps*x//10,:])
        plt.plot(dataGrid[-1,:])
        plt.show()
    y+=1
file="diffusion_free_ends_a_"+str(alpha)+"_t_"+str(tSteps)+"_x_"+str(xSteps)+".npy"
print(file)
np.save(file,dataGrid)

