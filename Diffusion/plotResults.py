# -*- coding: utf-8 -*-
"""
Created on Fri May 20 16:02:51 2022

@author: brade
"""
import numpy as np
import matplotlib.pyplot as plt
data = np.load("diffusion_hard_wall_a_0.4_t_20000_x_102.npy") # load a file

data= data[:,1:-1] # Trim data some
centerLocation = 99 # Location of interest, need not be center
timeSteps = 20000 # time steps of simulation
xSteps = 100 # x steps of simulation
dx = .3/(xSteps) # determine x-step size



#d=0.000000163103
d = 2.188e-7 #pvt
#d = 1.247e-7 #ps
print("pvt:")
alpha = 0.1
dt = alpha*dx**2/d
time = timeSteps*dt/(3600)
dataSum = np.sum(data, axis=1)
dataSum = dataSum/dataSum[0]
timeArray = np.linspace(0,time,timeSteps)
tHalf = timeArray[np.argmin(np.abs(dataSum-0.5))]
print("First t_1/2:", tHalf)


plt.figure()
plt.plot(np.linspace(0,time,timeSteps), data[:,centerLocation])
plt.xlabel("time (hours)")
plt.ylabel("oxygen fraction at inner edge")
#plt.xlim(2.5,9)
plt.show()

plt.figure()
plt.plot(np.linspace(0,time,timeSteps), data[:,centerLocation])
plt.yscale('log')
plt.xlabel("time (hours)")
plt.ylabel("oxygen fraction at inner edge")
#plt.xlim(2.5,9)
plt.show()

oxygenAmount = np.sum(data, axis=1)*dx

plt.figure()
plt.plot(np.linspace(0,time,timeSteps), oxygenAmount/oxygenAmount[0])
plt.xlabel("time (hours)")
plt.ylabel("total oxygen fraction")
#plt.ylim(.25,.5)
#plt.xlim(0,3)
#plt.xlim(2.5,9)
plt.show()

plt.figure()
plt.plot(np.linspace(0,time,timeSteps), oxygenAmount/oxygenAmount[0])
plt.yscale('log')
plt.xlabel("time (hours)")
plt.ylabel("total oxygen fraction")
#plt.xlim(2.5,9)
plt.show()


logRatio1 = np.log(dataSum)

logDerivative1 = (logRatio1[2:]- logRatio1[0:-2])/(2*dt)

plt.figure()
plt.plot(np.linspace(0,time,timeSteps)[1:-1], logDerivative1)


#plt.yscale('log')
plt.xlabel("time (hours)")
plt.ylabel("derivative of log oxygen fraction")
#plt.xlim(0,.8)
plt.ylim(-.0001,0)
plt.show()


plt.figure()
plt.plot(np.linspace(0,time,timeSteps)[1:-1], np.log(1/2)/logDerivative1/3600)


#plt.yscale('log')
plt.xlabel("time (hours)")
plt.ylabel("Tau_1/2 (hours)")
#plt.xlim(0,.8)
plt.ylim(0,24)
plt.show()
print("1/2 time",np.log(1/2)/logDerivative1[-1]/3600)