# -*- coding: utf-8 -*-
"""
Created on Fri May 20 15:27:40 2022

@author: brade
"""

import numpy as np
import matplotlib.pyplot as plt

atm_cm_hg = 76
o2_percent_tiles = .21
o2_percent_air = .13
solubility = .012
propConstant = 1/solubility
print("propConstant", propConstant)

tSteps = 12000
xSteps = 74
alpha = .2
iterations = 30000

dataGrid = np.ones([tSteps,xSteps])  # Something in material
dataGrid[:,0] = dataGrid[:,0] * 0 # Nothing along the edges
dataGrid[:,-1] = dataGrid[:,0]

dataGrid[:,32:42] = o2_percent_air/o2_percent_tiles # Something in the middle




def step(data):
    
    
    newData = np.copy(data) 
    newData[1:,1:31] = alpha*(data[0:-1, 2:32] + data[0:-1, 0:30]) + (1 -2*alpha) * data[0:-1,1:31]
    newData[1:,43:-1] = alpha*(data[0:-1, 44:] + data[0:-1, 42:-2])+ (1 -2*alpha)*data[0:-1,43:-1]
    
    newData[1:,32:42] = alpha*(data[0:-1, 33:43] + data[0:-1, 31:41]) + (1 -2*alpha) * data[0:-1,32:42]
    
    
    
    newData[1:,31] = alpha*(propConstant*data[0:-1,32]+data[0:-1,30]) + data[0:-1,31]*(1-alpha*(1+propConstant))
    
    
    newData[1:,42] = alpha*(data[0:-1,43]+propConstant*data[0:-1,41]) + data[0:-1,42]*(1-alpha*(1+propConstant))
    newData[1:,32:42] = np.repeat(np.expand_dims(np.mean(newData[1:,32:42], axis=1), axis=1),10, axis=1)
    
    
    return(newData)
    
maxChange = 10
y=0
while(maxChange>0): # Max change will become zero eventuallly
    newData = step(dataGrid)
    maxChange = np.max(np.abs(newData-dataGrid))
    dataGrid = newData
    data = np.copy(dataGrid)
    data[:,32:42] = data[:,32:42]*propConstant
    if((y+1)%400==0):
        print("Step", y+1)
        print("Max Change", maxChange)
        plt.figure()
        for x in range(10):
            plt.plot(data[tSteps*x//10,:])
        plt.plot(data[-1,:])
        plt.ylim(0,7)
        plt.show() 
    y+=1
file = "mid_gap_crank_prop_"+str(round(propConstant,2))+"_start_1000_a_"+str(alpha)
file +="_t_"+str(tSteps)+"_x_"+str(xSteps)+".npy"
np.save(file,dataGrid)



