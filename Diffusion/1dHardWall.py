# -*- coding: utf-8 -*-
"""
Created on Fri May 20 15:27:40 2022

@author: brade
"""

import numpy as np
import matplotlib.pyplot as plt

tSteps = 10000 # Total number of time steps
xSteps = 102 # Total number of x steps
alpha = .4  # Step size, should not be greater than 0.5

dataGrid = np.ones([tSteps,xSteps])  # Something in the material
dataGrid[:,0] = dataGrid[:,0]*0  # None on one side

def step(inputData):
    data = np.copy(inputData)
    data[1:,1:-1] = alpha*(data[0:-1, 2:] + data[0:-1, 0:-2]) + (1-2*alpha)*data[0:-1,1:-1]
    data[:,-1] = data[:,-2]  # hard wall condition
    return(data)


maxChange = 10
y=0
while(maxChange>0): # Max change will become zero eventuallly
    newData = step(dataGrid)
    maxChange = np.max(np.abs(newData-dataGrid))
    dataGrid = newData
    if((y+1)%400==0):
        print("Step", y+1)
        print("Max Change", maxChange)
        
        plt.figure()
        for x in range(10): # Summarize current results
            plt.plot(dataGrid[tSteps*x//10,:])
        plt.plot(dataGrid[-1,:])
        plt.show()
    y+=1
saveFile = "diffusion_hard_wall_a_"+str(alpha)+"_t_"+str(tSteps)+"_x_"+str(xSteps)+".npy"
print(saveFile)
np.save(saveFile, dataGrid) # Save current grid
 


