# HypoxicScintillator



## UI
The UI section of this repo consists of the UI used for interfacing with the multimeter and power supplies that will be used for the data acquistion. In summary, the UI is responsbile for setting up the scans to be run on the multimeter, setting the initial values for the power supply, and then displaying data to the user while letting them change settings. 

## Diffusion
The diffusion section of this repo contains code used to simulation to diffusion of oxygen into or out of the plastic scintilators studied. They help in the determination of how long it will take the scintilators to adapt to different levels of oxygen in their environment.
